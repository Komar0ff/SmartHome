		import React from 'react'
import Formsy from 'formsy-react';
import { Snackbar } from 'material-ui';
import SiteLayout from './SiteLayout.jsx'

export default class Login extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			message: "",
			showMessage: false
		};
	}

	onSubmit = (model) => {
		this.props.effi.auth({
			login: this.refs.loginInput.value,
			password: this.refs.passwordInput.value,
			success: () => {
				this.props.app.authenticated();
			},
			error: (resp) => {
				this.setState({
					showMessage: true,
					message: resp.ExceptionText
				});
			}
		});
	}

	render() {
		return (
			<SiteLayout app={this.props.app} effi={this.props.effi}>
				<section className="login-layout">
					<Formsy.Form ref="loginForm" onSubmit={this.onSubmit} className="login-form">
						<h3 className="login-form__header-label">Вход</h3>
						<div className="login-form__input-container">
							<label className="login-form__input-label">Email</label>
							<input placeholder="longemailname@gmail.com" type="text" name="login" className="login-form__input" ref="loginInput" />
						</div>
						<div className="login-form__input-container"><label className="login-form__input-label"></label>
							<input placeholder="qweasd" type="password" name="password" className="login-form__input" ref="passwordInput" />
							<div className="login-form__input-footer">
							<span className="login-form__error-message">{this.state.message}</span>
							<a className="login-form__link" href="#restore-password">Restore passwordь</a></div>
						</div>
						<button className="login-form__button" type="submit">Войти</button>
						<a href="#/signup" className="login-form__button login-form__button--last">Зарегистрироваться</a>
					</Formsy.Form>
				</section>
				{/*
				<Formsy.Form ref="loginForm" onSubmit={this.onSubmit}>
					<div className="fields-container">
						<h1>Вход</h1>
						<FormsyText className="text-field" name="login" floatingLabelText="Логин" required validations="isExisty" required validationError="Неверный логин" />
						<FormsyText className="text-field" name="password" floatingLabelText="Пароль" required validations="isExisty" required validationError="Неверный пароль" type="password" />
					</div>
					<div className="links-container">
						<a href="#/forgot-password" className="forgot-password">Забыли пароль</a>
					</div>
					<div className="submit-container">
						<RaisedButton className="login-button" primary label="Войти" type="submit" />
					</div>
					<div className="links-container">
						<a href="#/signup" className="signup">Зарегистрироваться</a>
					</div>
				</Formsy.Form>
				*/}
				<Snackbar
					open={this.state.showMessage}
					message={this.state.message}
					autoHideDuration={5000}
					onRequestClose={this.handleRequestClose}
				/>
			</SiteLayout>
		);
	}
}
