import React from 'react'

import 'styles/panel-menu.less';

export default class PanelMenu extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			menu_items: [
				{
					img: require('../../assets/img/menu/home.png'),
					url: '#/',
					title: 'Милый дом',
					cls: 'home'
				},
				{
					img: require('../../assets/img/menu/circle.png'),
					url: '#/act/Smarthouse/Graphics',
					title: 'Сводки',
					cls: 'circle'
				},
				{
					img: require('../../assets/img/menu/boxes.png'),
					url: '#/act/Smarthouse/Widgets',
					title: 'Виджеты',
					cls: 'boxes'
				},
				{
					img: require('../../assets/img/menu/share1.png'),
					url: '#/act/Smarthouse/Scenarios',
					title: 'Сценарии',
					cls: 'share1'
				},
				{
					img: require('../../assets/img/menu/share2.png'),
					url: '#/act/Smarthouse/Devices',
					title: 'Устройства',
					cls: 'share2'
				}

			],
			logout: {
				img: require('../../assets/img/menu/boxes2.png'),
				url: '#/',
				title: 'Устройства',
				cls: 'boxes2'
			}
		};
	}

	render() {
		return (
			<div className="page-layout__menu">
				<ul className="panel-menu">
					{this.state.menu_items.map(function(item, i) {
						let cls = 'list-item list-item--' + item.cls;
						return (
							<li key={i} className="panel-menu__list-item">
								<a href={item.url} className={cls}>
								
									<img src={item.img} class="list-item__image" />
									<span class="list-item__text">{item.title}</span>
								</a>

							</li>
						);
					})}
					<li className="panel-menu__list-item">
						<a href="#/" className="list-item list-item--boxes2" onClick={this.props.app.logout}>
							<img src={this.state.logout.img} class="list-item__image" />

							<span class="list-item__text">Выход</span>
						</a>
					</li>
				</ul>
			</div>
		);
	}
}