import React from 'react';

export default class RightPanel extends React.Component {
	render() {
		return (
			<div className="page-layout__panel">
				<div className={this.props.className}>
					{this.props.children}
				</div>
			</div>
		);
	}
}
