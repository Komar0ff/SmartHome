import React from 'react'
import { observer } from 'mobx-react';
import EditableWidgetDevice from '../../panel/components/EditableWidgetDevice';
import EditableWidgetInfo from '../../panel/components/EditableWidgetInfo';
import Weather from './Weather'
import appState from '../../stores/AppState';

import 'styles/infopanel/index.less';

function padz(n) {
	if (n.toString().length < 2) n = '0' + n;
	return n;
}
function getTime(now) {
	let h = now.getHours(),
		m = now.getMinutes(),
		time = padz(h) + ':' + padz(m);
	return time;
}
function getDate(now) {
	let d = now.getDate(),
		m = now.getMonth(),
		y = now.getFullYear(),
		date = padz(d) + '.' + padz(m+1) + '.' + y;
	return date;
}

const weekdays = ['Воскресение', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

@observer
export default class InfoPanel extends React.Component {
	render() {
		const now = new Date();
		return (
			<div className="infopanel">
				<div className="cell cell--type-1">
					<div className="timeTime">
						<div className="timeTime__time">
							<span className="time">{getTime(now)}</span>
							<i className="fa fa-sun-o"></i>
						</div>
						<div className="timeTime__date">
							<span className="date">{weekdays[now.getDay()]}, {getDate(now)}</span>
						</div>
					</div>
				</div>

				<div className="cell cell--type-1">
					<Weather />
				</div>

				<div className="cell cell__several-cell-container">
					<div className="cell cell--type-3">
						<div className="devices-power-consumption">
							<div className="devices-power-consumption__labels">
								<span>Активно устройств</span>
								<span>Расход кВт / ч</span>
							</div>
							<div className="devices-power-consumption__values">
								<span>9<span>/18</span></span>
								<span>12</span>
							</div>
						</div>
					</div>

					<div className="cell cell--type-3">
						<EditableWidgetDevice label="Ночник" img={require('../../../assets/img/night_light.png')} />
					</div>
				</div>

				<div className="cell__several-cell-container">
					<div className="cell cell--type-4">
						<EditableWidgetDevice label="Камера" img={require('../../../assets/img/camera.png')} additional_block='camera' />
					</div>
					<div className="cell cell--type-4-container">
						<div className="cell cell--type-5">
							<EditableWidgetDevice label="Потолочное освещение" img={require('../../../assets/img/night_light.png')} />
						</div>
						<div className="cell cell--type-5">
							<EditableWidgetInfo label="Расход" img={require('../../../assets/img/lightning.png')}  value="12" unit="кВт / ч" />
						</div>
					</div>
				</div>
			</div>
		);
	}
}
