import React from 'react'
import { observer } from 'mobx-react';
import appState from '../../stores/AppState';

import 'styles/infopanel/weather.less';

const weekdays = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];

function sameDay(d1, d2) {
	return d1.getFullYear() === d2.getFullYear()
		&& d1.getDate() === d2.getDate()
		&& d1.getMonth() === d2.getMonth();
}

@observer
export default class Weather extends React.Component {
	componentDidMount = () => {
		let me = this;
		appState.requestWeather();
	}

	render() {
		let now = new Date();
		let forecasts = [],
			forecast = appState.weather.forecast || [];
		for (let i=0; i<7; i++) {
			let date = new Date(now.getTime() + 24 * 60 * 60 * 1000 * i);
			let fcday = {day: weekdays[date.getDay()], prec_type: null, temp: null};
			for (let j=0; j<forecast.length; j++) {
				let fc = forecast[j];
				if (sameDay(date, fc.for_date)) {
					fcday.prec_type = fc.prec_type;
					fcday.temp = Math.round(fc.temp_day);
					break;
				}
			}
			forecasts.push(fcday);
		}

		return (
			<div className="weather">
				<div className="weather__top">
					<div className="weather__temperature">
						<img className="today__img" src={require("../../../assets/img/weather-rain.png")} />
						<span className="today__temperature-container">
							<span className="today__value">{Math.round(appState.weather.temp)}</span>&nbsp;
							<span className="today__unit">℃</span>
						</span>
					</div>
					<div className="weather__weather-params">
						<div className="weather-params__column weather-params__column--label">
							<div className="weather-params__label">Влажность</div>
							<div className="weather-params__label">Давление мм рт. ст.</div>
							<div className="weather-params__label">Ветер</div>
						</div>
						<div className="weather-params__column weather-params__column--values">
							<div className="weather-params__value-container weather-params__value-container--red">
								<span className="weather-params__value">{appState.weather.humidity}</span>
								<span className="weather-params__unit">%</span>
							</div>
							<div className="weather-params__value-container weather-params__value-container--green">
								<span className="weather-params__value">{Math.round(appState.weather.pressure_mm)}</span>
							</div>
							<div className="weather-params__value-container">
								<span className="weather-params__value">{appState.weather.wind_speed}</span>
								<span className="weather-params__unit">м / с</span>
							</div>
						</div>
					</div>
				</div>
				<div className="weather__bottom">
					{forecasts.map(function(fcday, i) {
						return <WeatherBottomDate key={i} label={fcday.day} value={fcday.temp} weather={fcday.prec_type} />;
					})}
				</div>
			</div>
		);
	}
}

class WeatherBottomDate extends React.Component {
	render() {
		let sun = require("../../../assets/img/weather-sun.png"),
			rain = require("../../../assets/img/weather-rain.png");
		let weather_ico = (this.props.weather == "rain" ? rain : sun);
		return (
			<div className="weather__date">
				<span className="date__day">{this.props.label}</span>
				<div className="date__temperature">
					<span>{this.props.value}</span>
					<span className="unit">℃</span>
				</div>
				<img className="date__img" src={weather_ico} />
			</div>
		);
	}
}
