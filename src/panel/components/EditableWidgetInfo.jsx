import React from 'react';
import { observer } from 'mobx-react';
import appState from '../../stores/AppState';

export default class EditableWidgetInfo extends React.Component {
	render() {
		let unit_class = this.props.unit_class || "editable-widget-info__unit";
		return (
			<div className="editable-widget editable-widget--info">
				<div className="editable-widget__info">
					<div className="editable-widget-info">
						<img src={this.props.img} className="editable-widget-info__img" />
						<div className="editable-widget-info__meta">
							<div className="editable-widget-info__value-content">
								<span className="editable-widget-info__value">{this.props.value}</span>&nbsp;
								<span className={this.unit_class}>{this.props.unit}</span>
							</div>
							<span class="editable-widget-info__label">{this.props.label}</span>
						</div>
					</div>
				</div>
				<RemoveOption />
			</div>
		);
	}
}

@observer
class RemoveOption extends React.Component {
	render() {
		if (!appState.edit_mode) return null;
		return (
			<div className="editable-widget__remove">
				<div className="remove-button">✖</div>
			</div>
		);
	}
}