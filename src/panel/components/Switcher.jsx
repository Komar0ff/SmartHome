import React from 'react';

import 'styles/common/switcher.less';

export default class Switcher extends React.Component {
	// constructor(props, context) {
	// 	super(props, context);

	// 	console.log(this.props.checked, this.props.checked || false)
	// 	this.state = {
	// 		checked: this.props.checked || false
	// 	}
	// }

	onChange = (e) => {
		let checked = !this.props.checked;
		// this.setState({
		// 	checked: checked
		// });
		if (this.props.onChange) this.props.onChange(checked);
	}

	render() {
		let on_label = this.props.on_label || "ВКЛ",
			off_label = this.props.off_label || "ВЫКЛ",
			on_cls = "power-state" + (this.props.checked ? " power-state--active" : ""),
			off_cls = "power-state" + (this.props.checked ? "" : " power-state--active");
		return (
			<div className={this.props.className}>
				<span className={on_cls}>{on_label}</span>
				<label className="switch">
					<input type="checkbox" onChange={this.onChange} checked={this.props.checked	} />
					<div className="slider round" />
				</label>
				<span className={off_cls}>{off_label}</span>
			</div>
		);
	}
}