import React from 'react';
import { observer } from 'mobx-react';
import appState from '../../stores/AppState';

export default class EditableWidgetDevice extends React.Component {
	render() {
		let additional_block = '', hr = '';
		if (this.props.additional_block == 'slider') additional_block = <SliderBlock />;
		else if (this.props.additional_block == 'camera') additional_block = <CameraBlock />;
		if (additional_block) hr = <hr/>;
		return (
			<div className="editable-widget editable-widget--device">
				<div className="editable-widget__device">
					<div className="editable-widget-device">
						<div className="editable-widget-device__label-content">
							<span className="editable-widget-device__label">{this.props.label}</span>
							<img src={this.props.img} className="editable-widget-device__label-img" />
						</div>
						<hr />
						<div className="editable-widget-device__power-state">
							<span className="power-state">ВКЛ</span>
							<label className="switch">
								<input type="checkbox" />
								<div className="slider round" />
							</label>
							<span className="power-state power-state--active">ВЫКЛ</span>
						</div>
						{hr}
						{additional_block}
					</div>
				</div>
				<RemoveOption />
			</div>
		);
	}
}

@observer
class RemoveOption extends React.Component {
	render() {
		if (!appState.edit_mode) return null;
		return (
			<div className="editable-widget__remove">
				<div className="remove-button">✖</div>
			</div>
		);
	}
}

class SliderBlock extends React.Component {
	render() {
		return (
			<div className="editable-widget-device__additional-info">
				<div className="additional-info">
					<div className="additional-info__value-container">
						<span className="additional-info__value">24</span>
						<span className="additional-info__unit">℃</span>
					</div>
					<div className="additional-info__min">0</div>
					<div className="additional-info__max">40</div>
					<div className="additional-info__slider">0</div>
				</div>
			</div>
		);
	}
}

class CameraBlock extends React.Component {
	render() {
		return (
			<div className="editable-widget-device__camera-state">
				<div className="camera-state">
					<div className="camera-state__signal-container">
						<span className="camera-state__state-name">Сигнал</span>
						<img className="camera-state__state-img" src={require("../../../assets/img/connection_quality.png")} />
					</div>
					<div className="camera-state__signal-container">
						<span className="camera-state__state-name">Запись</span>
						<img className="camera-state__state-img" src={require("../../../assets/img/green_circle.png")} />
					</div>
				</div>
			</div>
		);
	}
}