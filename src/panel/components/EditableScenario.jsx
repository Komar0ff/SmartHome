import React from 'react';
import Switcher from './Switcher';

export default class EditableScenario extends React.Component {
	render() {
		return (
			<div className="editing-script">
				<div className="form">
					<div className="form__header">
						<span>{this.props.title}</span>
					</div>
					<div className="form__content">
						<p>{this.props.text}</p>
					</div>
					<Switcher className="form__footer" />
				</div>
			</div>
		);
	}
}