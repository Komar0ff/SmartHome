import React from 'react';
import { observer } from 'mobx-react';
import appState from '../../stores/AppState';

import 'styles/common/content-tabs.less';

@observer
export default class ContentTabs extends React.Component {
	onClick = (tab, e) => {
		if (e) e.preventDefault();
		if (this.props.onClick) {
			this.props.onClick(tab, e);
		}
	}

	render() {
		let title = (this.props.title ? <div className="content-tabs__title">{this.props.title}</div> : null);
		let addBlock = (this.props.showAdd && this.props.onAdd ? <a className="content-tabs__add" onClick={this.props.onAdd}>+</a> : null);
		return (
			<section className="content__tabs">
				<div className="content-tabs">
					{title}
					{this.props.tabs.map(function(tab, i) {
						if (!appState.edit_mode && tab.edit_mode_only) return;
						let cls = (tab.active ? 'content-tabs__tab content-tabs__tab--active' : 'content-tabs__tab');
						let text = (tab.text ? tab.text : tab);
						return <a className={cls} key={i} onClick={(e) => {this.onClick(tab, e)}}>{text}</a>
					}, this)}
					{addBlock}
				</div>
			</section>
		);
	}
}