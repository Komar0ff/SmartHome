import React from 'react';
import PanelLayout from '../../panel/PanelLayout';
import RightPanel from '../../panel/components/RightPanel';
import appState from '../../stores/AppState';

import 'styles/Devices/index.less';
import 'styles/Devices/panel.less';

class DevicesList extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			sections: [
				{id: 0, name: 'Не размещено', active: true, devices: []}
			]
		};
	}

	componentDidMount = () => {
		this.refresh();
	}

	refresh = (floorid_open) => {
		let me = this;
		requestMyDevices(this.props.effi, floorid_open, (sections) => {
			me.setState({
				sections: sections
			});
			me.refs.layout.refresh(sections);
			if (me.props.onRefresh) me.props.onRefresh(sections);
		});
	}

	render() {
		return (
			<DevicesListLayout effi={this.props.effi} app={this.props.app} sections={this.state.sections} onClick={this.props.onClick} ref="layout" />
		);
	}
}

function requestMyDevices(effi, floorid_open, success) {
	appState.devices.requestMyDevices((devices) => {
		let smap = {},
			slist = [],
			free_devices = [];
		for (let i in devices) {
			let device = devices[i];
			if (!device.floorid) free_devices.push(device);
			else {
				if (!(device.floorid in smap)) {
					slist.push({id: device.floorid, name: device.floorname, active: (device.floorid == floorid_open), devices: []})
					smap[device.floorid] = slist.length-1;
				}
				slist[smap[device.floorid]].devices.push(device);
			}
		}
		slist.push({id: 0, name: 'Не размещено', active: true, devices: free_devices});
		if (success) success(slist);
	});
}

function getIconByKindCode(kindcode, on) {
	let icon = 'green_circle.png';
	switch (kindcode) {
		case 'iSWITCH': 
			icon = 'chip-green.png';
			break;
		case 'Lamp': 
			icon = (!!on ? 'night_light.png' : 'night_light_gray.png');
			break;
		case 'Rosette': 
			icon = (!!on ? 'rosette.png' : 'rosette_gray.png');
			break;
	}
	return require("../../../assets/img/" + icon);
}

class DevicesListLayout extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			sections: this.props.sections
		};
	}

	refresh = (sections) => {
		this.setState({
			sections: sections
		})
	}

	onClick = (device, e) => {
		if (e) e.preventDefault();
		if (this.props.onClick) {
			this.props.onClick(device, e);
		}
	}

	onClickSection = (section, e) => {
		if (e) e.preventDefault();
		let sections = this.state.sections;
		for (let i in sections) {
			if (sections[i].id != section.id) continue;
			sections[i].active = !sections[i].active;
			this.setState({
				sections: sections
			});
			break;
		}
	}

	onDeviceDelete = (device, e) => {
		if (e) e.preventDefault();
		if (this.props.onDeviceDelete) {
			this.props.onDeviceDelete(device, e);
		}
	}

	render() {
		return (
			<ul className="list">
			{this.state.sections.map(function (section, i) {
				let cls = "list-item list-item--parent";
				if (section.active) cls += " list-item--active";
				let iconClass = 'fa ' + (section.icon || '');
				return (
				<li key={section.id} className="list__item">
					<div className={cls} onClick={(e) => {this.onClickSection(section, e)}}>
						<i className="list-item__arrow fa fa-angle-down"></i>
						<div className="list-item__label">{section.name}</div>
						<div className="settings"><i className={iconClass}></i></div>
					</div>
					<ul className="list">
						{this.state.sections[i].devices.map(function (device, i) {
							let key = device.id;
							let icon = <img src={getIconByKindCode(device.kindcode)} className="list-item__img" />;
							if (this.props.onDeviceDelete) icon = <i class="fa fa-times" onClick={(e) => {this.onDeviceDelete(device, e)}}></i>;
							let label = `[${device.id}] ${device.name}`;
							if (device.name == device.kindname && device.uid) label += ` (${device.uid})`;
							return (
								<li key={key} className="list__item" onClick={(e) => {this.onClick(device, e)}}>
									<div className="list-item list-item--child">
										<div className="list-item__label">{label}</div>
										{icon}
									</div>
								</li>
							)
						}, this)}
					</ul>
				</li>
				);
			}, this)}
			</ul>
		);
	}
}

export { DevicesList, DevicesListLayout, requestMyDevices, getIconByKindCode };
