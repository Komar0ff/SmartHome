import React from 'react';
import Draggable from 'draggable';
import { observer } from 'mobx-react';
import appState from '../../stores/AppState';
import { getIconByKindCode } from './DevicesList';

import 'styles/common/content-tabs.less';

export default class DeviceInformer extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			active: false,
			showMenu: false,
			device: this.props.device
		};
	}

	componentDidMount() {
		if (this.props.draggable) {
			let drg = new Draggable(this.refs.informerRoot, {
				setPosition: false,
				grid: 10,
				limit: {
					x: [0, 700], 
					y: [0, 500]
				},
				onDragEnd: () => {
					if (this.props.onDragEnd) {
						const style = this.refs.informerRoot.style;
						let device = this.state.device;
						device.x_position = parseInt(style.left);
						device.y_position = parseInt(style.top);
						this.setState({
							device: device
						});
						this.props.onDragEnd(this.state.device, device.x_position, device.y_position);
					}
				}
			});
		}
	}

	onClick = (e) => {
		if (e) e.preventDefault();
		if (this.props.onClick) this.props.onClick(this.state.device);
	}

	onClickGear = (e) => {
		e.stopPropagation();
		if (e) e.preventDefault();
		if (this.props.onClickGear) this.props.onClickGear(this.state.device);
	}

	onRemove = (e) => {
		e.stopPropagation();
		if (e) e.preventDefault();
		if (this.props.onRemove) this.props.onRemove(this.state.device);
	}

	render() {
		let cls = "device-informer " + (this.props.className || "");
		if (this.state.device.__on__) cls += " device-informer--on";
		// if (this.state.active) cls += " device-informer--active";
		let style = {};
		if (this.state.device.x_position) style.left = `${this.state.device.x_position}px`;
		if (this.state.device.y_position) style.top =  `${this.state.device.y_position}px`;
		let menucls = "device-informer__menu";
		// if (this.state.active && this.state.showMenu) menucls += " device-informer__menu--active";
		let label = `[${this.state.device.id}] ${this.state.device.name}`;
		if (this.state.device.name == this.state.device.kindname && this.state.device.uid) label += ` (${this.state.device.uid})`;
		let icon = getIconByKindCode(this.state.device.kindcode, this.state.device.__on__);
		return (
			<div className={cls} style={style} onClick={this.onClick} onMouseDown={this.onMouseDown} ref="informerRoot">
				<div className="device-informer__img-container">
					<img className="device-informer__img" src={icon} />
				</div>
				<div className="device-informer__content">
					<p className="device-informer__text">{label}</p>
					<i className="fa fa-cog" onClick={this.onClickGear}></i>
				</div>
				<RemoveOption onClick={this.onRemove} />
			</div>
		);
	}
}

@observer
class RemoveOption extends React.Component {
	render() {
		if (!appState.edit_mode) return null;
		return (
			<div className="device-informer__remove" onClick={this.props.onClick}>
				<div className="remove-button">✖</div>
			</div>
		);
	}
}
