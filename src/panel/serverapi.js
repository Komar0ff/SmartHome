
export default class ServerApi() {
	constructor(backend) {
		this.backend = backend;
	}

	RequestMyDevices(success) {
		this.backend.request({
			url: '/srv/Smarthouse/Device/MyDeviceListGet',
			success: (data) => {
				let devices = prepareDataList(data),
					smap = {},
					slist = [],
					free_devices = [];
				for (let i in devices.data) {
					let device = devices.data[i];
					if (!device.floorid) free_devices.push(device);
					else {
						if (!(device.floorid in smap)) {
							slist.push({id: device.floorid, name: device.floorname, active: (device.floorid == floorid_open), devices: []})
							smap[device.floorid] = slist.length-1;
						}
						slist[smap[device.floorid]].devices.push(device);
					}
				}
				slist.push({id: 0, name: 'Не размещено', active: true, devices: free_devices});
				if (Defined(success)) success(slist);
			}
		});
	}
}