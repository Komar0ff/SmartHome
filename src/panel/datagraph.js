import * as d3 from "d3";

var VALUES = {
    SVG_WIDTH: 900,
    SVG_HEIGHT: 400,
    SPACE_TOP: 20,
    SPACE_LEFT: 30
};

function CountGraphYPosition(graph_nr, i) {
    return ((VALUES.SVG_HEIGHT - VALUES.SPACE_TOP*(graph_nr + 1)) / graph_nr + VALUES.SPACE_TOP) * i + VALUES.SPACE_TOP;
}

function CountGraphAreaHeight(graph_nr) {
    return (VALUES.SVG_HEIGHT - VALUES.SPACE_TOP*(graph_nr + 1)) / graph_nr;
}

function GetDateTime(datetime) {
    return new Date(datetime.adate.year, datetime.adate.month, datetime.adate.day, datetime.atime.hh, datetime.atime.mm, datetime.atime.ss);
}

function CreateGraph(selection, height, data) {
    var moments = [];
    var values = [];
    if (!data) {
        console.warn('empty data for graph');
        return;
    }
    data.forEach(function(ele) {
        // var amoment = GetDateTime(ele.t);
        moments.push(ele.t);
        values.push(ele.v);
    });
    var values_domain = [d3.min(values), d3.max(values)];
    var values_range = [height, 0];
    var y_scale = d3.scaleLinear()
                    .domain(values_domain)
                    .range(values_range);
    var y_axis = d3.axisLeft(y_scale);
    d3.select(selection)
        .append("g")
        .attr("class", "y_axis")
        .call(y_axis);
        
    var time_domain = [d3.min(moments), d3.max(moments)];
    var time_range = [0, VALUES.SVG_WIDTH - 2*VALUES.SPACE_LEFT];
    var x_scale = d3.scaleTime()
                .domain(time_domain)
                .range(time_range);
    var x_axis = d3.axisBottom(x_scale);
    d3.select(selection)
        .append("g")
        .attr("transform", "translate(0, " + height + ")")
        .attr("class", "x_axis")
        .call(x_axis);
    
    var area_width = VALUES.SVG_WIDTH - 2*VALUES.SPACE_LEFT;  
    
    d3.select(selection)
        .append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("style", "fill: #ffffff; fill-opacity: 0;  stroke: steelblue;")
        .attr("height", height)
        .attr("width", area_width)
        .on("mousemove", function () {
            //var x = d3.mouse(selection)[0]; //TODO
            //var y = d3.mouse(selection)[1];
            
        });
        
    var line = d3.line()
            .x(function(d) {
                return x_scale(d.t);
            })
            .y(function(d) {
                return y_scale(d.v);
            })
            .curve(d3.curveCatmullRom.alpha(0.5));
            
     var vertical_line = d3.select(selection)
        .append("line")
        .attr("style", "stroke: grey; stroke-width: 1px;")
        .attr("y1", 0)
        .attr("y2", height);
     var horizontal_line = d3.select(selection)
        .append("line")
        .attr("style", "stroke: grey; stroke-width: 1px;")
        .attr("x1", 0)
        .attr("x2", area_width);
        
     d3.select(selection)
        .append("g")
        .append("path")
        .datum(data)
        .attr("fill", "none")
        .attr("stroke", "red")
        .attr("stroke-width", 2)
        .attr("d", line);
}

class DataGraph {
    constructor(elem, oValue) {
        elem.JSControl = this;
        this.Parent = d3.select(elem);
        this.ID_ = this.Parent.attr("id");
        this.svg = this.Parent
                .append("div")
                .attr("class", "graph-layout")
                .attr("style", "border: 1px solid #eee;")
                .append("svg")
                .attr("width", VALUES.SVG_WIDTH)
                .attr("height", VALUES.SVG_HEIGHT);

        if (oValue) this.SetValue(oValue);
    }
    
    ID() {
        return this.ID_;
    }
    
    Value() {
        return this.Value_;
    }
    
    SetValue(oValue) {
        this.Value_ = oValue;
        var area_height = CountGraphAreaHeight(oValue.length);
        var area_width = VALUES.SVG_WIDTH - 2*VALUES.SPACE_LEFT;
        this.svg.selectAll("g.graph_area")
            .remove();

        var graph_area = this.svg.selectAll("g.graph_area")
            .data(oValue)
            .enter()
            .append("g")
            .attr("class", "graph_area")
            .attr("transform", function(d, i) {
                var translate = "translate(" + VALUES.SPACE_LEFT + ", ";
                translate += CountGraphYPosition(oValue.length, i);
                translate += ")";
                return translate;
            });        
            
        graph_area.each(function(data) {
           CreateGraph(this, area_height, data.points);
        });
        graph_area.each(function(data) {
            d3.select(this)
                .append("text")
                .attr("style", "fill: green; font-weight: bold; ")
                .attr("x", 0)
                .attr("y", -2)
                .text(function(d) {
                    return d.payload;
                });
        })
    }
    
    SetAttribute(sName, sValue) {
        
    }
}

export { VALUES, CreateGraph, DataGraph };
