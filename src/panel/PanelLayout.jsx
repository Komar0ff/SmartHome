import React from 'react'
import Header from './Header'
import PanelMenu from './PanelMenu'

export default class PanelLayout extends React.Component {
	render() {
		return (
			<div class="page">
				<PanelMenu app={this.props.app} effi={this.props.effi} />
				<div className="page-layout panel-layout">
					<Header app={this.props.app} effi={this.props.effi} />
					<div className="page-layout__content">
						{this.props.children}
						{this.props.panel}
					</div>
				</div>
			</div>
		);
	}
}