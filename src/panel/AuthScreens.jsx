import React from 'react'
import Formsy from 'formsy-react';
import { Decorator as FormsyElement } from 'formsy-react'
import { Snackbar } from 'material-ui';
import SiteLayout from './SiteLayout.jsx'

import 'styles/login.less';

@FormsyElement()
class InputField extends React.Component {
	render() {
		let cls = `${this.props.cls || 'login'}-form__input`,
			container_cls = `${cls}-container`,
			label_cls = `${cls}-label`,
			asterisk = (this.props.required ? <span class="form-required-asterisk">*</span> : null);
			// console.log('InputField', this.props.showError(), this.props.getErrorMessage(), this.props.getErrorMessages());
			// console.log('InputField', this.props.name, {this.props.showErrors ? <ErrorMessages messages={this.props.errorMessages} /> : null})
		return (
			<div className={container_cls}>
				<label className={label_cls}>{this.props.label} {asterisk}</label>
				<input type={this.props.type} name={this.props.name} className={cls} placeholder={this.props.placeholder}
					value={this.props.getValue()} onChange={(e) => this.props.setValue(e.target.value)} autoFocus={this.props.autoFocus} />
				{this.props.showError() ? <span className="login-form__input-error">{this.props.getErrorMessage()}</span> : null}
				{this.props.children}
			</div>
		);
	}
}

class LoginScreen extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			message: "",
			showMessage: false
		};
	}

	onSubmit = (model) => {
                this.props.app.authenticated();

		this.props.effi.auth({
			login: model.login,
			password: model.password,
			success: () => {
				this.props.app.authenticated();
			},
			error: (resp) => {
				this.setState({
					showMessage: true,
					message: resp.ExceptionText
				});
			}
		});
	}

	mapInputs = (inputs) => {
		return {
			login: inputs.login,
			password: inputs.password
		}
	}


	render() {
		return (
			<SiteLayout app={this.props.app} effi={this.props.effi}>
				<section className="login-layout">
					<Formsy.Form ref="loginForm" onSubmit={this.onSubmit} mapping={this.mapInputs} className="login-form">
						<h3 className="login-form__header-label">Вход</h3>
						<InputField label="Email" placeholder="longemailname@gmail.com" type="text" name="Логин" autoFocus />
						<InputField label="Пароль" type="password" name="password" className="login-form__input" placeholder="Password" >
							<div className="login-form__input-footer">
								<span className="login-form__error-message">{this.state.message}</span>
								<a className="login-form__link" href="#restore-password">Восстановить пароль</a>
							</div>
						</InputField>
						<button className="btn login-form__button" type="submit">Вход</button>
						<a className="btn login-form__button login-form__button--last" href="#/signup">Зарегистрироваться</a>
					</Formsy.Form>
				</section>
			</SiteLayout>
		);
	}
}

class RegisterScreen extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			canSubmit: false
		};
	}

	enableButton = () => {
		this.setState({
			canSubmit: true
		});
	}
	disableButton = () => {
		this.setState({
			canSubmit: false
		});
	}

	onSubmit = (model) => {
		console.log(model);
		let me = this;
		/*serializeAURLAsync(model, function(data) {
			me.props.backend.login({
				url: '/nologin/srv/Smarthouse/Person/Register_API',
				data: data,
				success: function (data) {
					me.props.success(`Ð ÐµÐ³Ð¸ÑÑ‚Ñ€Ð°Ñ†Ð¸Ñ ÑƒÑÐ¿ÐµÑˆÐ½Ð¾ Ð·Ð°Ð²ÐµÑ€ÑˆÐµÐ½Ð°. ÐŸÐµÑ€ÐµÐ¹Ð´Ð¸Ñ‚Ðµ Ð¿Ð¾ ÑÑÑ‹Ð»ÐºÐµ Ð½Ð° <a href="#/">Ð»Ð¾Ð³Ð¸Ð½ ÑÑ‚Ñ€Ð°Ð½Ð¸Ñ†Ñƒ</a> Ð¸ Ð²Ð¾Ð¹Ð´Ð¸Ñ‚Ðµ Ð² ÑÐ¸ÑÑ‚ÐµÐ¼Ñƒ Ð´Ð»Ñ Ð¿Ñ€Ð¾Ð´Ð¾Ð»Ð¶ÐµÐ½Ð¸Ñ Ñ€Ð°Ð±Ð¾Ñ‚Ñ‹.`);
				},
				error: function (error) {
					console.log(me.props);
					me.props.error(error.ExceptionText);
				}
			});
		});*/
		
	}

	render() {
		return (
			<Formsy.Form ref="loginForm" onValidSubmit={this.onSubmit} onValid={this.enableButton} onInvalid={this.disableButton} className="login-form register-form">
				<h3 className="login-form__header-label">Регистрация пользователя</h3>
				<InputField label="Имя" input placeholder="Введите имя" type="text" name="fullname" required />
				<InputField label="Телефон" placeholder="+7 916 455-75-55" type="text" name="phone" required />
				<InputField label="Email" placeholder="example@mail.ru" type="email" name="email" validations="isEmail" validationError="Введен неправильный адрес‚ email" required />
				<InputField label="Пароль" placeholder="Password" type="password" name="password" required />
				<InputField label="Повторите пароль" input placeholder="Password" type="password" name="password2" required />
				<button className="btn login-form__button" type="submit" enabled={!this.state.canSubmit}>Регистрация</button>
				<a className="btn login-form__button login-form__button--last" href="#/">Вход</a>
			</Formsy.Form>
		);
	}
}


class RestorePasswordScreen extends React.Component {
	onSubmit = (model) => {
		console.log(model);
	}

	render() {
		return (
			<SiteLayout app={this.props.app} effi={this.props.effi}>
				<section className="login-layout">
					<Formsy.Form ref="loginForm" onSubmit={this.onSubmit} className="login-form">
						<h3 className="login-form__header-label">Восстановление пароля</h3>
						<InputField label="Email" placeholder="longemailname@gmail.com" type="text" name="login" />
						<button className="btn login-form__button" type="submit">Восстановить пароль</button>
						<a className="btn login-form__button login-form__button--last" href="#/">Вход</a>
					</Formsy.Form>
				</section>
			</SiteLayout>
		);
	}
}

function BasicForm(Compenent) {
	return class extends React.Component {
		constructor(props, context) {
			super(props, context);

			this.state = {
				errorMessage: '',
				showError: false,
				successMessage: '',
				showSuccess: false
			};
		}

		showError = (message) => {
			this.setState({
				showError: true,
				errorMessage: message
			});
		}

		showSuccess = (message) => {
			this.setState({
				showSuccess: true,
				successMessage: message
			});
		}

		render() {
			const formStyles = (this.state.showSuccess ? {display: 'none'} : {});
			const successStyles = (this.state.showSuccess ? {} : {display: 'none'});
			return (
				<SiteLayout app={this.props.app} effi={this.props.effi}>
					<section className="login-layout"  style={formStyles}>
						<Compenent app={this.props.app} effi={this.props.effi} success={this.showSuccess} error={this.showError} />
					</section>
					<section className="login-layout success-layout" style={successStyles}>
						<div dangerouslySetInnerHTML={{__html: this.state.successMessage}} />
					</section>
					<Snackbar
						open={this.state.showError}
						message={this.state.errorMessage}
						autoHideDuration={5000}
						onRequestClose={this.handleRequestClose}
					/>
				</SiteLayout>
			);
		}
	}
}

let SignupScreen = BasicForm(RegisterScreen);

export { LoginScreen, SignupScreen, RestorePasswordScreen };
