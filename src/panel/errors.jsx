import React from 'react'
import SiteLayout from './SiteLayout.jsx'

class Screen404 extends React.Component {
	render() {
		return (
			<SiteLayout app={this.props.app} effi={this.props.effi}>
				<div className="panel-content">
					<h1>404 Страница не найдена</h1>
				</div>
			</SiteLayout>
		);
	}
}

export { Screen404 }
