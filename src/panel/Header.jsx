import React from 'react';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import { observer } from 'mobx-react';
import appState from '../stores/AppState';

@observer
export default class Header extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			showMenu: false
		}
	}

	toggleMenu = () => {
		let showMenu = this.state.showMenu;
		this.setState({showMenu: !showMenu});
	}

	logout = (e) => {
		if (e) e.preventDefault();
		this.props.app.logout();
	}

	showProfile = (e) => {
		window.location.href="#/act/Smarthouse/Profile";
	}

	toggleEditMode = (on) => {
		appState.edit_mode = on;
		this.toggleMenu();
	}

	render() {
		let menuStyle = {
			display: (this.state.showMenu ? "inline-block" : "none")
		}
		return (
				<header className="page-layout__header">
					<div className="page-header">
						<a href="#/"><img className="page-header__companyname" src={require("../../assets/img/IHS_logo.png")}/></a>

						<HeaderProfileInformer app={this.props.app} effi={this.props.effi} onClick={this.toggleMenu} />
						<div class="page-header__profile-menu-container" style={menuStyle}>
							<Menu desktop={true} class="page-header__profile-menu">
								<MenuItem primaryText="Режим просмотра" checked={!appState.edit_mode} insetChildren={true} onClick={() => {this.toggleEditMode(false)}} />
								<MenuItem primaryText="Режим редактирования" checked={appState.edit_mode} insetChildren={true} onClick={() => {this.toggleEditMode(true)}} />
								<Divider />
								<MenuItem primaryText="Редактировать профиль" onClick={this.showProfile} insetChildren={true} />
								<MenuItem primaryText="Выйти" onClick={this.logout} insetChildren={true} />
							</Menu>
						</div>
					</div>
				</header>
		);
	}
}

class HeaderProfileInformer extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			current_person: {id: 0, fullname: ''},
			showMenu: false
		}
	}

	 // componentDidMount() {
		// this.loadCurrentPerson();
	//}

	//loadCurrentPerson = () => {
		//let me = this;
		//appState.effi.request({
			//url: '/srv/Smarthouse/Person/MyGet',
			//success: (data) => {
				//me.setState({
					current_person: data
				//});
			//}
		//});
	//}

	render() {
		if (!this.state.current_person.id) return <div />;
		return (
			<div class="page-header__profile" onClick={this.props.onClick}>
				<div class="page-header__cog"><i class="fa fa-cog"></i></div>
				<div class="page-header__name">{this.state.current_person.fullname}</div>
				<img class="page-header__avatar" src="http://placehold.it/25x25" />
				<i class="page-header__arrow fa fa-angle-down"></i>

				
		{/*
			<div class="page-header__profile-menu list-menu dropdown">
				<div role="menu">
					<span role="menuitem">
						<div style={{marginLeft: "0", padding: "0 64px", position: "relative"}}>
							<svg viewBox="0 0 24 24" class="left-icon"><path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path></svg>
							<div>Режим просмотра</div>
						</div>
					</span>
				</div>
			</div>
		*/}
			</div>
		);
	}
}
