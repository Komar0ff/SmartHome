import React from 'react';
import Header from './Header'

export default class SiteLayout extends React.Component {
	render() {
		return (
			<div class="page-layout">
				<Header app={this.props.app} effi={this.props.effi} />
				<div class="page-layout__content">
					{this.props.children}
				</div>
			</div>
		);
	}
}