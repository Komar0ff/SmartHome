var host = ''; 

var backend = {
    user: {
        url: host + '/users',
        get: function(success, error) {
            $.ajax({
                url: URL + '/',
                method: 'GET',
                success: success, 
                error: error
            })   
        },
        update: function(fields, success, error) {
            $.ajax({ 
                url: URL + '/',
                method: 'UPDATE',
                headers: {'Content-type': 'application/json'},
                data: JSON.stringify(fields),
                success: success,
                error: error
            }) 
        },
        weather: function(opts) {}
    },
    devices: {
        url: host + '/devices',
        list:  function(){},
        get_last_data: function(){},
        remove: function(){},
        create: function(){}
    },
    floors: {
        url: host + '/floors',
        list: function(){}
    }
}
