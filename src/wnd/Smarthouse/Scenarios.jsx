import React from 'react';
import PanelLayout from '../../panel/PanelLayout';
import ContentTabs from '../../panel/components/ContentTabs';
import RightPanel from '../../panel/components/RightPanel';
import EditableScenario from '../../panel/components/EditableScenario';
import { DevicesList } from '../../panel/components/DevicesList';

import 'styles/scenarios.less';

export default class ScenariosScreen extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			scenarios: [],
		}
	}

	componentDidMount = () => {
		let me = this;
		// this.props.effi.request({
		// 	url: '/srv/Smarthouse/Floor/MyFloorListGet',
		// 	success: (data) => {

		// 	}
		// });
	}


	render() {
		return (
			<PanelLayout app={this.props.app} effi={this.props.effi}
						panel={<ScenariosPanel effi={this.props.effi} app={this.props.app} 
											ref="panel" />}>
				<div className="content">
					<header className="content__header"><h3>Сценарии</h3></header>

					<div className="content__editing-scripts">
						<div className="editing-scripts-layout">
							<EditableScenario title="Наименование сценария" text="Стратификация, если рассматривать процессы в рамках частно-правовой теории, аннигилирует филогенез." />
							<EditableScenario title="Наименование сценария" text="Дело в том, что идея правового государства откровенна." />
							<EditableScenario title="Наименование сценария" text="Акцентуированная личность, на первый взгляд, недоступно реквизирует бихевиор." />
							<EditableScenario title="Наименование сценария" text="Безусловно, марксизм неумеренно притягивает юридический задаток. Предсознательное гарантирует контраст." />
							<EditableScenario title="Наименование сценария" text="Безусловно, марксизм неумеренно притягивает юридический задаток. Предсознательное гарантирует контраст." />
							<EditableScenario title="Наименование сценария" text="Акцентуированная личность, на первый взгляд, недоступно реквизирует бихевиор." />
						</div>
					</div>
					
				</div>
			</PanelLayout>
		);
	}
}


class ScenariosPanel extends React.Component {

	render() {
		const tabs = [
			{ text: "Готовые", active: true },
			{ text: "Настроить", active: false }
		];
		return (
			<RightPanel className="edit-device-panel">
				<ContentTabs tabs={tabs} />
				<DevicesList effi={this.props.effi} 
							app={this.props.app} 
							onClick={this.props.onDeviceClick} 
							ref="devicesList" />
			</RightPanel>
		);
	}

}

