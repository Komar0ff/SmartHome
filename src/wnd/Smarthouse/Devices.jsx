import React from 'react';
import PanelLayout from '../../panel/PanelLayout';
import RightPanel from '../../panel/components/RightPanel';
import ContentTabs from '../../panel/components/ContentTabs';
import { DevicesList, DevicesListLayout } from '../../panel/components/DevicesList';


export default class DevicesScreen extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			sections: [
				{id: 1, name: 'Влажность', active: false, icon: 'fa-cog', devices: []},
				{id: 2, name: 'Датчики',   active: true,  icon: 'fa-cog',  devices: [{name: 'Сценарий 1'}, {name: 'Сценарйи 2'}]},
				{id: 3, name: 'Освещение', active: false, icon: 'fa-cog', devices: []}
			]
		};
	}

	onDeviceDelete = (device, e) => {
		console.log(device);
	}

	render() {
		return (
			<PanelLayout app={this.props.app} effi={this.props.effi} title="Устройства" panel={<DevicesPanel effi={this.props.effi} app={this.props.app} />}>
				<div className="content">
					<header className="content__header"><h3>Группировка приборов</h3></header>
					<div className="content__grouping-devices">
						<div className="grouping-devices">
							<DevicesListLayout effi={this.props.effi} app={this.props.app} sections={this.state.sections} onDeviceDelete={this.onDeviceDelete} />
						</div>
					</div>
				</div>
			</PanelLayout>
		);
	}
}

class DevicesPanel extends React.Component {
	render() {
		const tabs = [
			"Редактирование",
			{ text: "Устройства", active: true },
			"Графики", 
			"Сценарии",
			"Привязка",
			"Обои"
		];
		return (
			<RightPanel className="edit-device-panel">
				<ContentTabs tabs={tabs} />
				<DevicesList effi={this.props.effi} app={this.props.app} />
			</RightPanel>
		);
	}
}



// pg_dump -n mqtt -U postgres smarthouse > mqtt.db
// psql -a -p 5432 --username=postgres --command="DROP SCHEMA IF EXISTS mqtt CASCADE" smarthouse
// psql -p 5432 -f mqtt.db --username=postgres smarthouse

/*
Posrgresql 9.6 repo for Ubuntu 14.04
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-9.6
*/
