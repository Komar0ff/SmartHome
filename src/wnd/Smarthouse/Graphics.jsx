import React from 'react';
import PanelLayout from '../../panel/PanelLayout'
import { DevicesList } from '../../panel/components/DevicesList'
import { DataGraph } from '../../panel/datagraph'
import ContentTabs from '../../panel/components/ContentTabs'
import RightPanel from '../../panel/components/RightPanel';
import { Line } from 'react-chartjs';

import 'styles/graphics.less';

export default class GraphicsScreen extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			selected_device: { id: 0 },
			payloadlist: [],
			telemetry: {}
		}
	}

	componentDidMount() {
		if (this.state.selected_device.id) {
			this.updatePayloads();
		}
	}

	onDeviceClick = (device) => {
		this.setState({
			selected_device: device
		});
		this.updatePayloads(device.id);
	}

	onTabClick = (tab) => {
		let payloadlist = this.state.payloadlist;
		let changed = false;
		for (let i in payloadlist) {
			let curr = payloadlist[i];
			if (curr.id == tab.id) {
				if (!curr.active) {
					console.log(curr.id, 'ON');
					payloadlist[i].active = true;
					changed = true;
				}
			}
			else {
				if (curr.active) {
					console.log(curr.id, 'OFF');
					payloadlist[i].active = false;
					changed = true;
				}
			}
		}
		if (changed) {
			this.setState({
				payloadlist: payloadlist
			})
			this.updateGraph(this.state.selected_device.id, payloadlist);
		}
	}

	updatePayloads = (deviceid) => {
		let me = this;
		this.props.effi.request({
			url: '/srv/Smarthouse/Device/PayloadListGet',
			data: `id=i:${deviceid}&`,
			success: (data) => {
				let payloadlist = [];
				for (let i=1; i<data.length; i++) {
					payloadlist.push({text: data[i][0], active: i==1, id: data[i][0]});
				}
				me.setState({
					payloadlist: payloadlist
				});

				if (payloadlist.length > 0) {
					me.updateGraph(deviceid, payloadlist);
				}
			}
		});
	}

	updateGraph = (deviceid, payloads) => {
		let me = this;
		let serialized_payloads = '';
		for (let i=0; i<payloads.length; i++) {
			if (payloads[i].active) {
				serialized_payloads += `s:${payloads[i].id}&`
			}
		}
		this.props.effi.request({
			url: '/srv/Smarthouse/Device/DeviceDataGet',
			data: `id=i:${deviceid}&payloadlist=Value:Array:${serialized_payloads}&&`,
			success: (data) => {
				console.log(data);
				me.setState({
					telemetry: data
				});
			}
		});
	}

	render() {
		let comment = (this.state.selected_device.id ? `#${this.state.selected_device.id} ${this.state.selected_device.name}` : 'не выбрано. Выберите устройство на боковой панели');
		return (
			<PanelLayout app={this.props.app} effi={this.props.effi} panel={<GraphicsPanel effi={this.props.effi} app={this.props.app} />}>
				<div className="content">
					<header className="content__header">
						<h3>Сводки (графики)</h3>
					</header>
					<span>Устройство: {comment}</span>
					<ContentTabs tabs={this.state.payloadlist} telemetry={this.state.telemetry} onClick={this.onTabClick} />
					<Graph telemetry={this.state.telemetry} />
				</div>
			</PanelLayout>
		);
	}
}

class Graph extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			telemetry: {}
		}
	}

	componentDidMount() {
		// console.log(this.refs)
		// this.graph = new DataGraph(this.refs.graphContainer, this.props.telemetry);
	}

	componentDidUpdate() {
		console.log('update', this.props.telemetry);
		// this.graph.SetValue(this.props.telemetry);
	}

	render() {
		console.log(this.props.telemetry)
					// <div class="graph-container" ref="graphContainer" />
		let data = {
	        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
	        datasets: [{
	            label: '# of Votes',
	            data: [12, 19, 3, 5, 2, 3],
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(255, 206, 86, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(153, 102, 255, 0.2)',
	                'rgba(255, 159, 64, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255,99,132,1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)'
	            ],
	            borderWidth: 1
	        }]
	    };
	    let options = {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }]
	        }
	    };
		return (
			<div className="content__graphics">
				<div className="graphics">
					<Line data={data} options={options} redraw />
				</div>
			</div>
			
		);
	}
}

class GraphicsPanel extends React.Component {
	render() {
		const tabs = [
			"Редактирование",
			{ text: "Устройства", active: true}
		];
		return (
			<RightPanel className="edit-device-panel">
				<ContentTabs tabs={tabs} />
				<DevicesList effi={this.props.effi} app={this.props.app} />
			</RightPanel>
		);
	}
}
