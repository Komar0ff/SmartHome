import React from 'react';
import PanelLayout from '../../panel/PanelLayout'
import { DevicesList } from '../../panel/components/DevicesList'
import { DataGraph } from '../../panel/datagraph'
import ContentTabs from '../../panel/components/ContentTabs'
import RightPanel from '../../panel/components/RightPanel';
import { Line } from 'react-chartjs';

import 'styles/profile.less';

export default class Profile extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			selected_person: {id: 0, fullname: ''}
		}
	}

	componentDidMount() {
		this.loadPerson();
	}

	loadPerson = (id) => {
		let me = this;
		let method = (id ? 'MyJointUserGet' : 'MyGet');
		let args = (id ? `id=i:${id}&` : '');
		this.props.effi.request({
			url: '/srv/Smarthouse/Person/' + method,
			data: args,
			success: (data) => {
				me.setState({
					selected_person: data
				});
			}
		});
	}

	render() {
		return (
			<PanelLayout app={this.props.app} effi={this.props.effi} panel={<ProfilePanel effi={this.props.effi} app={this.props.app} />}>
				<div className="content">
					<header className="content__header">
						<h3>Редактирование профиля</h3>
					</header>
					<ProfileForm effi={this.props.effi} app={this.props.app} person={this.state.selected_person} />
				</div>
			</PanelLayout>
		);
	}
}

class ProfileForm extends React.Component {
	render() {
		return (
			<div className="content__edit-profile">
				<div className="edit-profile">
					<div className="edit-profile__avatar-section">
						<div className="avatar-section">
							<img className="avatar-section__img" src="http://placehold.it/170x170" />
							<button className="avatar-section__upload-btn btn">Загрузить</button>
							<span className="avatar-section__user-id">ID: {this.props.person.id}</span>
						</div>
					</div>

					<div className="edit-profile__personal-data">
						<div className="personal-data">
							<div className="form">
								<div className="form__header">
									<div className="form__header-label">Личные данные</div>
									<img className="form__header-img" src={require("../../../assets/img/man_contour.png")} />
								</div>
								<div className="form__content">
									<div className="form__group">
										<input type="text" className="form__input" required name="fullname" value={this.props.person.fullname} />
										<label className="form__label">Имя</label>
									</div>
								</div>
								<div className="form__footer">
									<div className="form__role-title">Роль</div>
									<div className="form__role-name">{this.props.person.user_role}</div>
								</div>
							</div>
						</div>
					</div>

					<div className="edit-profile__security">
						<div className="security">
							<div className="form">
								<div className="form__header">
									<div className="form__header-label">Безопасность</div>
								</div>
								<div className="form__content">
									<div className="form__const-data">
										<span className="form__const-data-title">Email</span>
										<span className="form__const-data-value">{this.props.person.email}</span>
									</div>
									<button className="security__btn btn">Изменить</button>
									<div className="form__group">
										<input type="password" className="form__input" required value="******" />
										<label className="form__label">Пароль</label>
									</div>
									<button className="security__btn btn">Изменить</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		);
	}
}

class ProfilePanel extends React.Component {
	render() {
		const tabs = [
			{ text: "Администраторы", active: true},
			{ text: "Пользователи", active: false}
		];
		return (
			<RightPanel className="edit-profile-panel">
				{/*<ContentTabs tabs={tabs} />*/}
				<PersonsList effi={this.props.effi} app={this.props.app} />
			</RightPanel>
		);
	}
}

class PersonsList extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			persons: []
		};
	}

	componentDidMount() {
		this.refresh();
	}

	refresh = () => {
		let me = this;
		this.props.backend.user.get_joined((data) => {
		    me.setState({
		        persons: data
		    });
		})
	}

	onClick = (person, e) => {
		if (e) e.preventDefault();
		if (this.props.onClick) {
			this.props.onClick(device, e);
		}
	}

	onDelete = (person, e) => {
		if (e) e.preventDefault();
		if (this.props.onDelete) {
			this.props.onDelete(person, e);
		}
	}

	render() {
		return (
			<ul className="list">
			{this.state.persons.map(function (person, i) {
				// let cls = "list-item list-item--parent";
				// if (section.active) cls += " list-item--active";
				// let iconClass = 'fa ' + (section.icon || '');
				return (
				<li key={person.id} className="list__item">
					<div className="list-item list-item--parent" onClick={(e) => {this.onClick(person, e)}}>
						{/*<i className="list-item__arrow fa fa-angle-down"></i>*/}
						<div className="list-item__label">{person.fullname}</div>
						<img className="list-item__img" src={require("../../../assets/img/trash.png")} onClick={this.onDeleteUser} />
					</div>
				</li>
				);
			}, this)}
			</ul>
		);
	}
}
