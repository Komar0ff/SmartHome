import React from 'react';
import PanelLayout from '../../panel/PanelLayout'

export default class Dashboard extends React.Component {
	render() {
		return (
			<PanelLayout app={this.props.app} effi={this.props.effi}>
				<div className="v-split-container">
					<div className="left-side">
						<div className="content">
							<header className="content__header"><h3>Пульт управления домашними роботами</h3></header>
						</div>
					</div>
					<div className="right-side">
						<h2>Боковая панель</h2>
					</div>
				</div>
			</PanelLayout>
		);
	}
}