import React from 'react';
import PanelLayout from '../../panel/PanelLayout';
import ContentTabs from '../../panel/components/ContentTabs';
import RightPanel from '../../panel/components/RightPanel';
import DeviceInformer from '../../panel/components/DeviceInformer';
import InfoPanel from '../../panel/components/InfoPanel';
import { DevicesList, requestMyDevices } from '../../panel/components/DevicesList';
import { DeviceInfo } from './DeviceEdit';
import appState from '../../stores/AppState';
import { getDeviceInterface } from '../../stores/DeviceStore';
import { observer } from 'mobx-react';

import 'styles/house.less';
import 'styles/widgets.less';

@observer
export default class House extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			sections: [],
			current_devices: []
		}
	}

	componentDidMount = () => {
		let me = this;
		appState.house.requestFloors((floors) => {
			me.setState({
				current_devices: this.findDevicesOnFloor(appState.house.current_floor.id, this.state.sections)
			});

			requestMyDevices(this.props.effi, appState.house.current_floor.id, (sections) => {
				me.onDevicesRefresh(sections);
			});
		});

		appState.devices.subscribe(this.onDeviceStateUpdated);
	}

	getTabs = () => {
		let tabs = [];
		for (let i=0; i<appState.house.floors.length; i++) {
			let floor = appState.house.floors[i];
			let active = (floor.id == appState.house.current_floor.id);
			tabs.push({text: floor.name, active: active, id: floor.id});
		}
		return tabs;
	}

	onDevicesRefresh = (sections) => {
		this.setState({
			sections: sections,
			current_devices: this.findDevicesOnFloor(appState.house.current_floor.id, sections)
		});
	}

	onDeviceStateUpdated = (device) => {
		let devs = this.state.current_devices;
		for (let i in devs) {
			let dev = devs[i];
			if (dev.id == device.id) devs[i] = device;
		}
		this.setState({current_devices: devs});
	}

	onFloorSelect = (tab) => {
		for (let i=0; i<appState.house.floors.length; i++) {
			let floor = appState.house.floors[i];
			if (floor.id == tab.id) {
				appState.house.current_floor = floor;
			}
		}
		this.setState({
			current_devices: this.findDevicesOnFloor(appState.house.current_floor.id, this.state.sections)
		});
	}

	findDevicesOnFloor = (floorid, sections) => {
		for (let i in sections) {
			if (sections[i].id == floorid) {
				return sections[i].devices;
			}
		}
		return [];
	}

	onPanelDeviceClick = (device) => {
		let me = this;
		const floorid = appState.house.current_floor.id;
		appState.devices.addDeviceOnFloor({
			floorid: floorid,
			device: device,
			success: (data) => {
				me.refs.panel.refreshDevices(floorid);
			}
		});
	}

	onFloorDeviceClick = (device) => {
		let idevice = getDeviceInterface(device);
		if (idevice.onClick) idevice.onClick();
	}

	onFloorDeviceClickGear = (device_) => {
		let device = device_;
		if (device.parentid) {
			device = appState.devices.findMyDevice(device.parentid);
		}
		this.refs.panel.openDevice(device);
	}

	onFloorDeviceRemove = (device) => {
		let me = this;
		const floorid = appState.house.current_floor.id;
		appState.devices.removeDeviceFromFloor({
			floorid: floorid,
			device: device,
			success: (data) => {
				let devs = me.state.current_devices;
				for (let i in devs) {
					if (devs[i].id == device.id) {
						devs.splice(i, 1);
						me.setState({current_devices: devs});
					}
				}
				me.refs.panel.refreshDevices(floorid);
			}
		});
	}

	onFloorAdd = () => {
		this.refs.panel.openFloorAdd();
	}
	onDeleteFloor = () => {
		console.log('root onDeleteFloor');
	}

	render() {
		return (
			<PanelLayout app={this.props.app} effi={this.props.effi}
						panel={<HousePanel effi={this.props.effi} app={this.props.app} 
											onDeviceClick={this.onPanelDeviceClick} 
											onDevicesRefresh={this.onDevicesRefresh}
											onDeleteFloor={this.onDeleteFloor}
											current_floor={appState.house.current_floor}
											ref="panel" />}>
				<div className="content">
					<header className="content__header"><h3>План помещения</h3></header>
					<ContentTabs title="Этажи:" tabs={this.getTabs()} onClick={this.onFloorSelect} showAdd={appState.edit_mode} onAdd={this.onFloorAdd} />
					<HouseFloor floor={appState.house.current_floor} devices={this.state.current_devices}
								effi={this.props.effi} app={this.props.app} 
								onDeviceClick={this.onFloorDeviceClick} onDeviceClickGear={this.onFloorDeviceClickGear} onDeviceRemove={this.onFloorDeviceRemove} />
				</div>
			</PanelLayout>
		);
	}
}

@observer
class HouseFloor extends React.Component {
	onDragDeviceInformer = (device, x, y) => {
		let me = this;
		const floorid = this.props.floor.id;
		this.props.effi.request({
			url: '/srv/Smarthouse/Device/MyPlace',
			data: `id=i:${device.id}&floorid=i:${floorid}&x_position=i:${device.x_position}&y_position=i:${device.y_position}`,
			success: (data) => {
				// me.refs.panel.refreshDevices(floorid);
			}
		});
	}

	render() {
		const floorimg = this.props.floor.floorimg_url || require('../../../assets/img/home-plan.png');
		return (
			<div className="content__room">
				<div className="room-layout">
					<div className="room-layout__map">
						<img src={floorimg} />
						{this.props.devices.map((device, i) => {
							return <DeviceInformer app={this.props.app} effi={this.props.effi} 
													key={device.id} device={device} 
													draggable={true} onDragEnd={this.onDragDeviceInformer} 
													onClick={this.props.onDeviceClick} onClickGear={this.props.onDeviceClickGear} onRemove={this.props.onDeviceRemove} />
						})}
					</div>
				</div>
			</div>
		);
	}
}


class EditFloorPanel extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			floor: this.props.floor,
			floorname: (this.props.floor ? this.props.floor.name : ''),
			floorimg: null
		};
	}

	handleChangeName = (event) => {
		this.setState({
			floorname: event.target.value
		});
	}
	// handleChangeImg = (event) => {
	// 	this.setState({
	// 		floorimg: event.target.value
	// 	});
	// }

	onSubmit = (e) => {
		e.preventDefault();
		const files = this.refs.floorimg.files;
		const file = (files.length>0 ? files[0] : null);
		if (this.props.add) appState.house.addFloor(this.state.floorname, file);
		else appState.house.updateFloor(this.state.floor.id, this.state.floorname, file);
	}

	onDelete = (e) => {
		e.preventDefault();
		appState.house.deleteFloor(this.state.floor.id, this.props.onDelete);
	}

	render() {
		let title = (this.props.add ? 'Новый этаж' : 'Изменить этаж');
		let del_button = (this.props.add ? null : <input type="button" value="Удалить" className="btn" onClick={this.onDelete} />);
		return (
			<div class="panel-information-block">
			<form class="floor-settings-form" onSubmit={this.onSubmit}>
				<div class="input-container">
					<label for="" class="input-label">{title}</label>
					<input type="text" name="floorname" class="floor-settings-form__input thin-input" value={this.state.floorname} onChange={this.handleChangeName} />
				</div>
				<br/><br/>
				<div class="input-container">
					<label for="" class="input-label">Изображение/планировка</label>
					<input type="file" name="floorimg" class="floor-settings-form__input thin-input" ref="floorimg" />
				</div>
				<div className="panel-information-buttons">
					<input type="submit" value="Сохранить" className="btn" />
					{del_button}
				</div>
			</form>
			</div>
		);
	}
}

@observer
class HousePanel extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.tabs = {
			devices: <DevicesList effi={this.props.effi} 
									app={this.props.app} 
									onClick={this.props.onDeviceClick} 
									onRefresh={this.props.onDevicesRefresh} 
									ref="devicesList" />,
			infopanel: <InfoPanel />,
			floor: <EditFloorPanel floor={this.props.current_floor} onDelete={this.onDeleteFloor} />
		}
		this.state = {
			tabs: [
				{ text: "Инфопанель", active: true,  id: 'infopanel' },
				{ text: "Устройства", active: false, id: 'devices' },
				{ text: "Этаж",       active: false, id: 'floor', edit_mode_only: true }
			],
			selected_device: {},
			add_floor: false
		}
	}

	refreshDevices = (floorid_open) => {
		this.refs.devicesList.refresh(floorid_open);
	}

	onTabClick = (tab) => {
		this.tabs.floor = <EditFloorPanel floor={this.props.current_floor} onDelete={this.onDeleteFloor} />;
		let tabs = this.state.tabs;
		for (let i in tabs) {
			tabs[i].active = (tabs[i].id == tab.id);
		}
		this.setState({
			tabs: tabs,
			selected_device: {},
			add_floor: false
		});
	}

	openTab = (id) => {
		this.onTabClick({id: id});
	}
	closeTab = () => {
		this.onTabClick({id: 'infopanel'});
	}

	onDeleteFloor = () => {
		this.closeTab();
		if (this.props.onDeleteFloor) this.props.onDeleteFloor();
	}


	activePanel = () => {
		if (this.state.selected_device.id) {
			const href = "#/act/Smarthouse/DeviceEdit?id=" + this.state.selected_device.id;
			return (
				<DeviceInfo device={this.state.selected_device}>
					<a href={href} class="btn">Изменить настройки</a>
				</DeviceInfo>
			);
		}
		if (this.state.add_floor) {
			return <EditFloorPanel add={true} onDelete={this.onDeleteFloor} />
		}
		let tabs = this.state.tabs;
		for (let i in tabs) {
			if (tabs[i].active) {
				return this.tabs[tabs[i].id];
			}
		}
		return null;
	}

	openDevice = (device) => {
		this.setState({
			selected_device: device
		});
	}

	openFloorAdd = () => {
		this.setState({
			add_floor: true
		});
	}

	render() {
		return (
			<RightPanel className="edit-device-panel">
				<ContentTabs tabs={this.state.tabs} onClick={this.onTabClick} />
				{this.activePanel()}
			</RightPanel>
		);
	}
}

