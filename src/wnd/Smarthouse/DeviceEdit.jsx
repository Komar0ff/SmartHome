import React from 'react';
import PanelLayout from '../../panel/PanelLayout';
import Switcher from '../../panel/components/Switcher';
import appState from '../../stores/AppState';

// import 'styles/house.less';
// import 'styles/widgets.less';

export default class DeviceEdit extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			deviceid: this.props.url_parameters.id,
			device: {id: this.props.url_parameters.id}
		}
	}

	componentDidMount() {
		let me = this;
		this.props.effi.request({
			url: '/srv/Smarthouse/Device/MyGet',
			data: `id=i:${this.state.deviceid}&`,
			success: (data) => {
				me.setState({
					device: data
				});
				this.refs.editPanel.refreshDeviceState(data);
			}
		});
	}

	render() {
		return (
			<PanelLayout app={this.props.app} effi={this.props.effi}>
				<div className="content">
					<header className="content__header"><h3>Устройство: {this.state.device.name}</h3></header>
					<ISwitchEdit device={this.state.device} ref="editPanel" />
				</div>
			</PanelLayout>
		);
	}

}


class ISwitchEdit extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			device: this.props.device,
			networks: [
				{label: 'Home WiFi', value: 1},
				{label: 'Network 2', value: 2},
				{label: 'Network 3', value: 3},
				{label: 'Network 4', value: 4},
			],
			device_types: [
				{label: 'Розетка 1', value: 1},
				{label: 'Розетка 2', value: 2},
				{label: 'Выключатель 1', value: 3},
				{label: 'Выключатель 2', value: 4},
			],
			time_zones: [
				{label: 'UTC+03:00 Московское время', value: 3}
			]
		}
	}

	refreshDeviceState = (device) => {
		this.setState({
			device: device
		});
		this.refs.info.refreshDeviceState();
	}

	onSubmit = (e) => {
		e.preventDefault();
		console.log('submit:', this, arguments);
	}

	onCancel = (e) => {
		e.preventDefault();
		appState.history.goBack();
	}

	render() {
		return (
			<form class="device-settings-form" onSubmit={this.onSubmit}>
				<div class="device-settings-fields">
					<div class="device-settings-col">
						<div class="device-settings-block">
							<h3 class="device-settings-block__header">Параметры подключения к WI-FI сети</h3>
							<div class="device-settings-form__input-container">
								<label class="device-settings-form__input-label">Сеть Wi-Fi</label>
								<Select name="network_select" options={this.state.networks} />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Пароль</label>
								<input type="password" name="password" class="device-settings-form__input thin-input" />
							</div>
							<div class="device-settings-form__input-container">
								<div class="label-with-switch">
									<label for="" class="device-settings-form__input-label">Статичный IP</label>
									<label class="switch">
										<input name="static_ip_enabled" type="checkbox" value="on" />
										<div class="slider round"></div>
									</label>
								</div>

								<input type="text" name="static_ip" class="device-settings-form__input thin-input" />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Сетевая маска</label>
								<input type="text" name="netmask" class="device-settings-form__input thin-input" />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Шлюз</label>
								<input type="text" name="gateway" class="device-settings-form__input thin-input" />
							</div>
						</div>
						<div class="device-settings-block">
							<h3 class="device-settings-block__header">Настройки MQTT</h3>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Сервер</label>
								<input type="text" name="mqtt_name" class="device-settings-form__input thin-input" />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Логин</label>
								<input value="user@mail.ru" type="email" name="mqtt_email" class="device-settings-form__input thin-input" />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Пароль</label>
								<input type="password" name="mqtt_password" class="device-settings-form__input thin-input" />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Порт</label>
								<input type="text" name="mqtt_port" class="device-settings-form__input thin-input" />
							</div>
						</div>
					</div>
					<div class="device-settings-col">
						<div class="device-settings-block">
							<h3 class="device-settings-block__header">Настройки пользователя</h3>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Логин</label>
								<input type="email" name="user_email" class="device-settings-form__input thin-input" />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Пароль</label>
								<input type="password" name="user_password" class="device-settings-form__input thin-input" />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Имя устройства</label>
								<input type="text" name="device_name" class="device-settings-form__input thin-input" />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Тип Устройства</label>
								<Select name="device_select" options={this.state.device_types} />
							</div>
							<div class="device-settings-form__input-container">
								<label for="" class="device-settings-form__input-label">Часовой пояс</label>
								<Select name="time_select" options={this.state.time_zones} />
							</div>
						</div>
						<div class="device-settings-block">
							<h3 class="device-settings-block__header">Загрузка прошивки</h3>
							<div class="device-settings-form__input-container">
								<label for="file_upload" class="device-settings-form__input-label">Выбрать файл</label>
								{/*<input type="text" disabled value="имя файла" class="device-settings-form__input thin-input file-input-label" onClick={() => {this.refs.file_upload.click()}} />*/}
								<label for="file_upload" class="btn device-settings-form__input" style={{width: '200px', margin: 0}}>Выбрать файл</label>
								<input type="file" name="file_upload" id="file_upload" class="hidden" />
							</div>
						</div>
						<input name="upload_button" type="button" value="Загрузить" class="btn" style={{margin: 0}}/>
					</div>
				</div>
				<ISwitchInfo device={this.state.device} ref="info">
					<input type="submit" value="Сохранить" className="btn" />
					<input type="button" value="Отмена" className="btn" onClick={this.onCancel} />
				</ISwitchInfo>
			</form>
		);
	}
}

class Select extends React.Component {
	constructor(props, context) {
		super(props, context);

		let value = this.props.value || (this.props.options.length>0 ? this.props.options[0].value : null);
		this.state = {
			show_options: false,
			value: value
		}
	}

	getLabel = (value) => {
		if (!this.props.options) return null;
		for (let i in this.props.options) {
			let opt = this.props.options[i];
			if (opt.value == value) return opt.label;
		}
	}

	onClick = () => {
		let show_options = this.state.show_options;
		this.setState({
			show_options: !show_options
		})
	}

	onSelect = (opt) => {
		this.setState({
			value: opt.value
		});
		if (this.props.onSelect) this.props.onSelect(opt);
	}

	render() {
		let label = this.getLabel(this.state.value);
		let options = (this.state.show_options ? (
				<ul class="options-list">
					{this.props.options.map((opt, i) => {
						let cls = (opt.value == this.state.value ? "active" : "");
						return (<li class={cls} value={opt.value} key={opt.value} onClick={() => {this.onSelect(opt)}}>{opt.label}</li>);
					}, this)}
				</ul>
			) : "");
		return (
			<div class="form__select thin-select" id="time_select" onClick={this.onClick}>
				<i className="fa fa-angle-down"></i>
				<div class="selected" value={this.state.value}>{label}</div>
				{options}
				<input type="hidden" name={this.props.name} value={this.state.value} ref="input" />
			</div>
		);
	}
}

const localized_months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

function humanTime(date) {
	let hours = date.getHours().toString();
	if (hours.length < 2) hours = '0'+hours;
	let minutes = date.getMinutes().toString();
	if (minutes.length < 2) minutes = '0'+minutes;
	let seconds = date.getSeconds().toString();
	if (seconds.length < 2) seconds = '0'+seconds;
	return hours+':'+minutes+':'+seconds;
}

function humanDate(date) {
	return date.getDate() + ' ' + localized_months[date.getMonth()] + ' ' + date.getFullYear() + ' ' + humanTime(date);
}

class ISwitchInfo extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			out1: 0,
			out2: 0,
			uptime: 0
		}
	}

	componentDidMount() {
		if (this.props.device.id) this.refreshDeviceState();
		// appState.effi.subscribe(this, 'OnTelemetryRefresh', this.onTelemetryRefresh);
		appState.devices.subscribe(this.onDeviceStateUpdated);
	}

	// componentWillUnmount() {
	// 	appState.effi.unsubscribe(this);
	// }

	// onTelemetryRefresh = (data) => {
	// 	if (data.Data.id) {
	// 		this.refreshDeviceState();
	// 	}
	// }

	onDeviceStateUpdated = (device, payload) => {
		if (device.id == this.props.device.id) {
			this.setState({
				out1: payload.sensors.out1 || 0,
				out2: payload.sensors.out2 || 0,
				uptime: payload.sensors['uptime:']
			});
		}
	}

	refreshDeviceState = () => {
		appState.devices.requestState(this.props.device.id, (data) => {
			if (data.sensors) {
				this.setState({
					out1: data.sensors.out1 || 0,
					out2: data.sensors.out2 || 0,
					uptime: data.sensors['uptime:']
				});
			}
		});
	}

	onChangeOut1 = (checked) => {
		this.setState({
			out1: checked
		});
		const cmd = (checked ? 'on' : 'off');
		appState.devices.sendCommand(this.props.device.id, `/${this.props.device.uid}/out1/${cmd}`);
	}

	onChangeOut2 = (checked) => {
		this.setState({
			out2: checked
		});
		const cmd = (checked ? 'on' : 'off');
		appState.devices.sendCommand(this.props.device.id, `/${this.props.device.uid}/out2/${cmd}`);
	}

	humanUptime = () => {
		const uptime = new Date(new Date().valueOf() - this.state.uptime * 1000);
		return humanDate(uptime);
	}

	render() {
		return (
			<div class="panel-device-information-block">
				<h3>{this.props.device.name}</h3>
				<ul class="panel-device-information">
					<li>ID: {this.props.device.uid}</li>
					<li>UpTime: {this.humanUptime()}</li>
					<li>LocalTime: {humanDate(new Date())}</li>
					<li>VDC</li>
					<li>MQTT Server connect status</li>
					<li>Версия прошивки</li>
					<li>Статус пользователя</li>
					<li>Дата окончания регистрации</li>
					<li>Тип устройства: розетка</li>
					<li>Часовой пояс: UTC+3:00 Московское время</li>
				</ul>
				<div class="panel-device-switches">
					<div class="panel-device-block">
						<span>Выход 1</span>
						<Switcher className="editable-widget-device__power-state" checked={this.state.out1 == 1} onChange={this.onChangeOut1} />
					</div>
					<div class="panel-device-block">
						<span>Выход 2</span>
						<Switcher className="editable-widget-device__power-state" checked={this.state.out2 == 1} onChange={this.onChangeOut2} />
					</div>
				</div>
				{/*
				<div class="device-settings-buttons">
					<input name="save_button" type="submit" value="Сохранить" class="system-form__button"/>
					<input name="sign_out_button" type="button" value="Выйти" class="system-form__button"/>
				</div>
				*/}
				<div className="panel-device-information-buttons">
					{this.props.children}
				</div>
			</div>
		);
	}
}

export { ISwitchInfo as DeviceInfo };
