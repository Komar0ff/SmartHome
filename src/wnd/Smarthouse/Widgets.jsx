import React from 'react';
import PanelLayout from '../../panel/PanelLayout'
import ContentTabs from '../../panel/components/ContentTabs'
import EditableWidgetInfo from '../../panel/components/EditableWidgetInfo'
import EditableWidgetDevice from '../../panel/components/EditableWidgetDevice'
import Masonry from 'masonry-layout'
import RightPanel from '../../panel/components/RightPanel';
import { DevicesList } from '../../panel/components/DevicesList'

import 'styles/widgets.less';

export default class WidgetsScreen extends React.Component {
	constructor(props, context) {
	super(props, context);

	let me = this;
		this.state = {
			widgets: [
				'Зал',
				'Потребление ресурсов',
				'Уличное освещение',
				{ text: 'Детская', active: true},
				'Домашнее освещение',
				'Кухня',
				'Ванная',
				'Камеры',
				'Спальня'
			]
		};
	}

	componentDidMount = () => {
		let me = this;
		let grids = document.querySelectorAll('.editable-widget-layout__category-content');
		for (let i in grids) {
			let msnry = new Masonry(grids[i], {
				itemSelector: '.editable-widget',
				// columnWidth: 220
			});
			setTimeout(function() {
				msnry.layout();
			}, 1000);
		}
	}

	render() {
		return (
			<PanelLayout app={this.props.app} effi={this.props.effi} title="Виджеты" panel={<WidgetsPanel effi={this.props.effi} app={this.props.app} />}>
				<div className="content">
					<header className="content__header"><h3>Виджеты</h3></header>
					<ContentTabs tabs={this.state.widgets} />
					
					<div className="content__editable-widget">
						<div className="editable-widget-layout">
							<div className="editable-widget-layout__category">
								<h3 className="editable-widget-layout__category-label">Информация</h3>
								<section className="editable-widget-layout__category-content">
									<EditableWidgetInfo img={require("../../../assets/img/blob.png")} label="Влажность" value="34" unit="%" />
									<EditableWidgetInfo img={require("../../../assets/img/thermometer.png")} label="Температура" value="21" unit="℃" unit_class="editable-widget-info__unit editable-widget-info__unit--thermometer" />
									<EditableWidgetInfo img={require("../../../assets/img/lightning.png")} label="Расход" value="12" unit="кВт / ч" />
								</section>
							</div>
							<div className="editable-widget-layout__category">
								<h3 className="editable-widget-layout__category-label">Устройства</h3>
								<section className="editable-widget-layout__category-content">
									<EditableWidgetDevice img={require("../../../assets/img/night_light.png")} label="Ночник" />
									<EditableWidgetDevice img={require("../../../assets/img/heater.png")} label="Нагреватель" additional_block="slider" />
									<EditableWidgetDevice img={require("../../../assets/img/camera.png")} label="Камера" />
									<EditableWidgetDevice img={require("../../../assets/img/rosette.png")} label="Розетка" />
								</section>
							</div>
						</div>
					</div>
				</div>
			</PanelLayout>
		);
	}
}


class WidgetsPanel extends React.Component {
	render() {
		const tabs = [
			"Редактирование",
			{ text: "Устройства", active: true}
		];
		return (
			<RightPanel className="edit-device-panel">
				<ContentTabs tabs={tabs} />
				<DevicesList effi={this.props.effi} app={this.props.app} />
			</RightPanel>
		);
	}
}
