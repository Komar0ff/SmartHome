import React from 'react';
import ReactDOM from 'react-dom';
// import Modal from 'react-modal';
import createHistory from 'history/createHashHistory'
import Route from 'route-parser'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// import { IconButton, RaisedButton, FlatButton, Paper, MenuItem, Divider, Dialog, AutoComplete, Snackbar} from 'material-ui';
// import { FormsyCheckbox, FormsyDate, FormsyRadio, FormsyRadioGroup, 
//          FormsySelect, FormsyText, FormsyTime, FormsyToggle, FormsyAutoComplete } from 'formsy-material-ui/lib';
import injectTapEventPlugin from 'react-tap-event-plugin';

// import { backend } from 'src/lib/backend.js'

import appState from 'stores/AppState';
import PanelLayout from 'panel/PanelLayout'
import { LoginScreen, SignupScreen, RestorePasswordScreen } from 'panel/AuthScreens'
import { Screen404 } from 'panel/errors'
import Dashboard from 'wnd/Smarthouse/Dashboard'
import House from 'wnd/Smarthouse/House'

import 'styles/panel.less'


function initHistory() {
    const history = createHistory();
    const unlisten = history.listen(onHistoryChange);
    return history;
}


let host = getCurrentScriptUrl();
let history = initHistory();
appState.setup({
    host: host,
    history: history
});
let backend = appState.backend;

let app = null;

function onHistoryChange(location, action) {
    if (app == null) return;
    let params = urlParameters(location.search);
    // console.log(action, location.pathname);
    if (location.pathname == '/') {
        app.openDefaultScreen(params);
        return;
    }
    if (location.pathname == '/signup') {
        return app.openScreen(SignupScreen, false, params);
    }
    if (location.pathname == '/restore-password') {
        return app.openScreen(RestorePasswordScreen, false, params);
    }
    if (location.pathname == '/restore-password/confirm') {
        return app.openScreen(ConfirmRestorePasswordScreen, false, params);
    }
    const screen_route = new Route('/act/*container/*screen');
    let m = screen_route.match(location.pathname);
    // console.log('  onHistoryChange:', location, m, params);
    if (m) {
        let wnd;
        try {
            wnd = require(`wnd/${m.container}/${m.screen}`);
        }
        catch (err) {
            console.warn(err.message);
            app.openScreen(Screen404, false, params);
            return;
        }
        app.openScreen(wnd.default, true, params);
    }
    else {
        app.openScreen(Screen404, false, params);
    }
}

function urlParameters(search) {
    var query_string = {};
    var query = search.substring(1);
    var vars = query.split("&");
    for (var i=0; i<vars.length; i++) {
        var pair = vars[i].split("=");
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
            query_string[pair[0]] = arr;
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    } 
    return query_string;
}

class PanelApp extends React.Component {

    constructor(props, context) {
        super(props, context);

        let me = this;
        this.state = {
            is_authenticated: false,
            screen: null,
            requires_auth: true,
            person: null
        };

         //backend.get({
          //  success: (data) => {
            //    me.setState({
              //       is_authenticated: true,
                //     person: data
                // });

            //}

        //});

    }

    componentWillMount() {
        injectTapEventPlugin();
    }

    openScreen = (screen, requires_auth, params) => {
        this.setState({
            screen: screen,
            requires_auth: requires_auth,
            url_parameters: params
        });
    }

    openDefaultScreen = (params) => {
        this.openScreen(House, true, params);
    }

    authenticated = () => {
        this.setState({
            is_authenticated: true
        });
    }

    // logout = (e) => {
    //     console.log('logout', e);
    //     if (e) e.preventDefault();
    //     backend.logout();
    //     this.setState({
    //         is_authenticated: false
    //     });
    // }

    render() {
        let Screen = (this.state.is_authenticated || !this.state.requires_auth ? this.state.screen : LoginScreen);
        // console.log(this.state.requires_auth, Screen);
        if (!Screen) Screen = Screen404;
        return (
            <div className="panel-wrapper">
            <MuiThemeProvider>
                <Screen app={this}  url_parameters={this.state.url_parameters} />
            </MuiThemeProvider>
            </div>
        );
    }
}


function getCurrentScriptUrl() {
    let scripts = document.getElementsByTagName('script');
    let index = scripts.length - 1;
    for (; index>=0; index--) {
        let script = scripts[index],
            addr_pos = script.src.indexOf('/assets/panel.js');
        if (addr_pos > 0) {
            return script.src.substr(0, addr_pos);
        }
    }
    return null;
}

function bodyAppend(elem) {
    let b = document.getElementsByTagName('body');
    if (b.length < 1) throw "There is no <body> element. ";
    b[0].appendChild(elem);
}

function findOrCreateRootElement(htmlid_) {
    let htmlid = htmlid_ || 'smarthouse-panel-body';
    let elem = document.getElementById(htmlid);
    if (!elem) {
        elem = document.createElement("div");
        elem.setAttribute('id', htmlid);
        document.body.appendChild(elem);
    };
    return elem;
}


(function showPanel () {
    let htmlid = 'smarthouse-panel';
    let elem = findOrCreateRootElement(htmlid);
    app = ReactDOM.render(
        <PanelApp host={host} />,
        elem
    );
    onHistoryChange(history.location, 'POP');
    return app;
})();

