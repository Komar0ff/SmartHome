import { observable, computed } from 'mobx';
import appState from './AppState';

export default class HouseStore {
	@observable floors = [];
	@observable current_floor = {};

	requestFloors(success) {
		let me = this;
		appState.backend.floors.list({
			success: (data) => {
				me.floors = data;
				me.selectFirstFloor();
				if (success) success(me.floors);
			}
		});
	}

	selectFirstFloor() {
		if (!this.current_floor.id && this.floors.length>0) {
			this.current_floor = this.floors[0];
		}
	}
	selectFloor(index) {
		console.log("selectFloor");
		for (let i=index; i>=0; i--) {
			if (this.floors.length>i) {
				this.current_floor = this.floors[i];
				break;
			}
		}
	}

	addFloor(floorname, file, success) {
		let me = this;
			appState.backend.floors.create({
                                params: {
                                    name: floorname,
                                    floorimg: file
                                },
				success: (data) => {
					me.floors.push(data)
					if (success) success(data);
				}
			});
	}

	updateFloor(id, floorname, file, success) {
		let me = this;
			appState.backend.floors.update({
                                params: {
                                    id: id,
                                    name: floorname,
                                    floorimg: file
                                },
				success: (data) => {
					let floor = me.findFloorById(id);
					if (floor) {
						floor.name = floorname;
						floor.floorimg_url = data.floorimg_url;
					}
					if (success) success(data);
				}
			});
	}

	deleteFloor(id, success) {
		let me = this;
		appState.backend.floors.remove({
                        params: {
                            id: id
                        },
			success: (data) => {
				let i = me.findFloorIndexById(id);
				me.floors.splice(i, 1);
				me.selectFloor(i-1);
				if (success) success(data);
			}
		});
	}

	findFloorById(id) {
		for (let i=0; i<this.floors.length; i++) {
			if (this.floors[i].id == id) return this.floors[i];
		}
	}
	findFloorIndexById(id) {
		for (let i=0; i<this.floors.length; i++) {
			if (this.floors[i].id == id) return i;
		}
	}
}
