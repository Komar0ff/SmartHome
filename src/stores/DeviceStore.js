import { observable, computed } from 'mobx';
import { Backend } from '../lib/backend'
import appState from './AppState';

function getRandomCoords(device) {
	return {
		x: device.x_position ? device.x_position : Math.round(Math.random() * 700 / 50) * 50,
		y: device.y_position ? device.y_position : Math.round(Math.random() * 500 / 50) * 50
	}
}

export default class DeviceStore {
	my_devices = [];
	_subscribers = [];

	constructor() {
	//	appState.effi.subscribe(this, 'OnTelemetryRefresh', this.onTelemetryRefresh);
	}

	findMyDevice(id) {
		for (let i=0; i<this.my_devices.length; i++) {
			if (this.my_devices[i].id == id) return this.my_devices[i];
		}
	}

	findChildOnPort(id, port) {
		for (let i=0; i<this.my_devices.length; i++) {
			let device = this.my_devices[i]
			if (device.parentid == id && device.parent_port == port) return device;
		}
	}

	requestMyDevices(success) {
		let me = this;
		appState.backend.devices.list({
                    success: (data) => {
		        me.my_devices = data;
                        this.requestAllStates();
		        if (success) success(me.my_devices);
                    }
		});
	}

	addDeviceOnFloor(opts) {
		const floorid = opts.floorid;
		const device = opts.device;
		const coords = ((opts.x && opts.y) ? {x: opts.x, y: opts.y} : getRandomCoords(device));
		if (device.floorid == floorid) return;
		appState.backend.devices.create({
                        params: {
                            device_id: device.id,
                            floor_id: floorid,
                            x_position: coords.x,
                            y_position: coords.y
                        },
			success: (data) => {
				if (opts.success) opts.success(data);
			}
		});
	}

	removeDeviceFromFloor(opts) {
		const device = opts.device;
		appState.backend.devices.remove({
                        params: {
                            device_id: device.id 
                        },
			success: (data) => {
				device.floorid = null;
				if (opts.success) opts.success(data);
			}
		});
	}

	requestState(id, success) {
		let me = this;
		appState.backend.devices.get_last_data({
                        params: {
                            device_id: id
                        },
			success: (data) => {
				this.notifySubscribers(this.findMyDevice(id, data), data);
				me.setupChildren(id, data);
				if (success) success(data);
			}
		});
	}

	setupChildren(id, payload) {
		let sensors = payload.sensors || {};
		for (let port in sensors) {
			let child = this.findChildOnPort(id, port);
			if (child) {
				child.__on__ = !!sensors[port];
				this.notifySubscribers(child, payload);
			}
		}
	}

	notifySubscribers(device, payload) {
		for (let i in this._subscribers) {
			let fn = this._subscribers[i];
			fn(device, payload);
		}
	}

	requestAllStates() {
		for (let i=0; i<this.my_devices.length; i++) {
			let device = this.my_devices[i];
			if (device.parentid) continue;
			this.requestState(device.id)
		}
	}

	onTelemetryRefresh(data) {
		if (data.Data.id) {
			let payload = JSON.parse(data.Data.payload);
			let device = this.findMyDevice(data.Data.id, data);
			this.notifySubscribers(device, payload);
			this.setupChildren(data.Data.id, payload);
		}
	}

	sendCommand(deviceid, topic, success) {
		let me = this;
		appState.backend.devices.send_command({
                        params: {
                            target_device: deviceid,
                            topic: topic
                        },
			success: (data) => {
				if (success) success(data);
			}
		});
	}

	subscribe(fn) {
		this._subscribers.push(fn);
	}
}

class iSwitchDevice {
	constructor(device) {
		this.device = device || {};
		if (this.parentid) {
			this.events = {
				onclick: this.switchOut
			}
		}
	}

	switchOut(port, on) {
		const cmd = (!!on ? 'off' : 'on');
		let port_ = port || this.port;
		appState.devices.sendCommand(this.device.id, `/${this.device.uid}/${port}/${cmd}`);
	}
}

class LampDevice {
	constructor(device) {
		this.device = device;
	}

	onClick() {
		let parent_device = appState.devices.findMyDevice(this.device.parentid);
		let parent = getDeviceInterface(parent_device);
		parent.switchOut(this.device.parent_port, this.device.__on__);
	}
}

class RosetteDevice extends LampDevice {

}

function getDeviceInterface(device) {
	switch(device.kindcode) {
		case 'iSWITCH': return new iSwitchDevice(device);
		case 'Lamp': return new LampDevice(device);
		case 'Rosette': return new RosetteDevice(device);
		default: throw `Unknown device kind: ${device.kindcode}.`;
	}
}

export { getDeviceInterface };
