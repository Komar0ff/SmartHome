import { observable, computed } from 'mobx';
import { backend } from 'lib/backend';
import DeviceStore from './DeviceStore';
import HouseStore from './HouseStore';

class AppState {
	@observable is_authenticated = false;
	@observable devices = new DeviceStore();
	@observable house = new HouseStore();
	@observable edit_mode = false;
	@observable weather = {};

	constructor() {
		this.setup();
	}

	setup (){
		
			this.backend = backend;
				
	}

	requestWeather(success) {
		let me = this;
		appState.backend.user.get_weather({
			success: (data) => {
				me.weather = data;
				if (success) success(me.floors);
			}
		});
	}


}

const appState = new AppState();
export default appState;
