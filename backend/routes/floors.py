from flask import Blueprint, request, g, jsonify
from models import Floor
from app import db


bp = Blueprint('floors', __name__)

@bp.route('/', methods=['GET'])
def list_floors():
    '''
    Old URL: /srv/Smarthouse/Floor/MyFloorListGet
    '''


    return jsonify([i.serialize() for i in g.floors.all()])


@bp.route('/', methods=['POST'])
def add_floor():
    '''
    Old URL: /srv/Smarthouse/Floor/MyAdd
    Request data: name(str), img(file)
    '''
    number = request.args.get('number')
    name = request.args.get('name')
    url = request.args.get('url')

    if number and name and url == None:
        return('Bad_request')
    else:
        db.session.add(Floor(
            number=number,
            name=name,
            plan_url=url))
        db.session.commit()

        return 'Add a new floor ( number: {}, name: {}, plan_url:{}' \
        .format(
            number,
            name,
            url)



@bp.route('/<floor_id>', methods=['PUT'])#PUT<- UPDATE
def update_floor(floor_id):
    '''
    Old URL: /srv/Smarthouse/Floor/MyUpdate
    Request data: name(str), img(file)
    '''
    number = request.args.get('number')
    name = request.args.get('name')
    url = request.args.get('url')

    if number and name and url == None:
        return('Bad_request')
    else:
        floor = g.floors.get(floor_id)
        floor.number=number
        floor.name=name
        floor.plan_url=url
        db.session.commit()



        return 'Update floor () number: {}, name: {}, plan_url:{}' \
        .format(
            number,
            name,
            url)

@bp.route('/<floor_id>', methods=['DELETE'])
def delete_floor(floor_id):
    '''
    Old URL: /srv/Smarthouse/Floor/MyDelete
    '''
    del_floor = g.floors.get_or_404(floor_id)
    db.session.delete(del_floor)
    db.session.commit()
    return 'Floor {} deleted'.format(floor_id)
