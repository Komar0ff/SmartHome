from flask import Blueprint, request, jsonify, g, current_app


bp = Blueprint('users', __name__) 

@bp.route('/', methods=['GET']) 
def user(): 
    '''
    Old URL: /srv/Smarthouse/Person/MyGet
    Merged with old URL: /srv/Smarthouse/Person/MyJointUserListGet
    '''

    return jsonify(g.user.serialize())

@bp.route('/', methods=['UPDATE']) 
def update_user(): 
    data = request.get_json()

    for key, value in data.items(): 
        setattr(g.user, key, value) 

    return 'Profile updated successfyly'

@bp.route('/weather', methods=['GET']) 
def get_current_weather():
    '''
    Old URL: /srv/Smarthouse/WeatherCity/WeatherGet_API
 
    Return weather for user's region
    TODO: Check me.weather usage in frontend
    '''

    return 'Current weather: ...'
