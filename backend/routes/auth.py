from flask import Blueprint 


bp = Blueprint('auth', __name__)

@bp.route('/login', methods=['POST']) 
def login(): 
    '''
    Old URL: /nologin/srv/Smarthouse/Person/Register_API 
    '''
    return 'Login...'
