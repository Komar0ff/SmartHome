from flask import Blueprint, g, jsonify, request
from sqlalchemy import or_
from models import Device
from app import db


bp = Blueprint('devices', __name__) 

@bp.route('/', methods=['GET'])
def get_devices_list():
    '''
    Old URL: /srv/Smarthouse/Device/MyDeviceListGet
    '''
    floor_id = request.args.get('floor_id')
    type = request.args.get('type')
    if floor_id == None and type == None:
        return jsonify([i.serialize() for i in g.devices.all()])
    elif floor_id == None and type != None:
        return  jsonify([i.serialize() for i in g.devices.filter_by(type=type).all()])
    elif floor_id != None and type == None:
        return  jsonify([i.serialize() for i in g.devices.filter_by(floor_id=floor_id).all()])
    else:
        return  jsonify([i.serialize() for i in g.devices.filter_by(floor_id=floor_id,type=type).all()])


@bp.route('/', methods=['POST'])
def add_device():
    '''
    Old URL: /srv/Smarthouse/Device/MyPlace

    Request data: id, floor_id, x_position, y_position

    '''
    floor_id = request.args.get('floor_id')
    type = request.args.get('type')
    x_position = request.args.get('x_position')
    y_position = request.args.get('y_position')
    if floor_id and type and x_position and y_position == None:
        return('Bad_request')
    else:
        db.session.add(Device(
            floor_id=floor_id,
            x_position=x_position,
            y_position=y_position,
            type=type))

        db.session.commit()

        return 'Add a new device ( floor_id: {}, x_position: {}, y_position: {},type:{}' \
        .format(
            floor_id,
            x_position,
            y_position,
            type)

@bp.route('/<device_id>', methods=['GET'])
def get_device(device_id):

    #return 'Device {} information: {}'.format(device_id,jsonify(g.devices.get(device_id).serialize()))
    return jsonify(g.devices.get_or_404(device_id).serialize())
@bp.route('/<device_id>', methods=['DELETE'])
def delete_device(device_id):
    '''
    Old URL: /srv/Smarthouse/Device/MyUnplace
    '''
    del_device = g.devices.get_or_404(device_id)
    db.session.delete(del_device)
    db.session.commit()
    return 'Device {} deleted'.format(device_id)

@bp.route('/<device_id>/data', methods=['GET'])
def get_device_data(device_id):
    '''
    Old URL: /srv/Smarthouse/Device/DeviceDataGet

    '''
    return '???'

@bp.route('/<device_id>/last_data', methods=['GET'])
def get_device_last_data(device_id):
    '''
    Old URL: /srv/Smarthouse/Device/DeviceDataGetLast
    '''
    return 'TEST'

@bp.route('/<device_id>/payloads', methods=['GET'])
def payload_list(device_id):
    '''
    ???
    '''
    return 'TEST'
