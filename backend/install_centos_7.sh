# Prepere install

yum -y -q update

# Install Python 3.6 and pip3

yum -y -q install yum-utils
yum -y -q groupinstall development
yum -y -q install https://centos7.iuscommunity.org/ius-release.rpm
yum -y -q install python36u
yum -y -q install python36u-pip
yum -y -q install python36u-devel
yum -y -q install openssl-devel
ln -s /usr/bin/pip3.6 /usr/bin/pip3

# Install nginx, add configuration

yum -y -q install epel-release
yum -y -q install nginx

echo "server {" > /etc/nginx/conf.d/smarthouse.conf
echo "    listen 80;" >> /etc/nginx/conf.d/smarthouse.conf
echo "    server_name localhost;" >> /etc/nginx/conf.d/smarthouse.conf
echo "    location / {" >> /etc/nginx/conf.d/smarthouse.conf
echo "        include    uwsgi_params;" >> /etc/nginx/conf.d/smarthouse.conf
echo "        uwsgi_pass unix:/run/smarthouse/uwsgi.sock;" >> /etc/nginx/conf.d/smarthouse.conf
echo "    }" >> /etc/nginx/conf.d/smarthouse.conf 
echo "}" >> /etc/nginx/conf.d/smarthouse.conf
systemctl restart nginx

# Clone project from git 

yum -y -q install git 
cd /var/www
rm -rf frontend/
rm -rf smarthouse/
git clone https://slushkov@bitbucket.org/IHSystem/frontend.git -q
mv frontend smarthouse  
cd smarthouse
git checkout py_backend

# Install & create virtualenv 

pip3 -q install virtualenv
cd /var/www/smarthouse/backend
virtualenv .venv -p python3.6 -q 
.venv/bin/pip3 -q install -r requirements.txt

# Install uwsgi & add config & add service 

pip3 -q install uwsgi
mkdir -p /etc/uwsgi/vassals

echo "[uwsgi]" > /etc/uwsgi/vassals/smarthouse.ini
echo "chdir           = /var/www/smarthouse/backend" >> /etc/uwsgi/vassals/smarthouse.ini
echo "module          = app:app" >> /etc/uwsgi/vassals/smarthouse.ini
echo "home            = /var/www/smarthouse/backend/.venv" >> /etc/uwsgi/vassals/smarthouse.ini
echo "socket          = /run/smarthouse/uwsgi.sock" >> /etc/uwsgi/vassals/smarthouse.ini
echo "chmod-socket    = 666" >> /etc/uwsgi/vassals/smarthouse.ini
echo "master          = true" >> /etc/uwsgi/vassals/smarthouse.ini
echo "vacuum          = true" >> /etc/uwsgi/vassals/smarthouse.ini
echo "uid          = smarthouse" >> /etc/uwsgi/vassals/smarthouse.ini
echo "gid          = uwsgi" >> /etc/uwsgi/vassals/smarthouse.ini


echo "[Unit]" > /etc/systemd/system/smarthouse-uwsgi.service
echo "Description=Smarthouse UWSGI service" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "[Service]" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "KillSignal=SIGQUIT" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "Type=notify" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "StandardError=syslog" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "NotifyAccess=all" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "ExecStartPre=/usr/sbin/groupadd --force uwsgi" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "ExecStartPre=/bin/bash -c 'if ! getent passwd smarthouse ; then /usr/sbin/useradd smarthouse -g uwsgi -M; fi'" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "ExecStartPre=/bin/mkdir -p /run/smarthouse/" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "ExecStartPre=/bin/chown -R smarthouse:uwsgi /run/smarthouse/" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "ExecStartPre=/bin/mkdir -p /var/log/smarthouse/" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "ExecStartPre=/bin/chown -R smarthouse:uwsgi /var/log/smarthouse/" >> /etc/systemd/system/smarthouse-uwsgi.service
echo "ExecStart=/usr/bin/uwsgi --ini /etc/uwsgi/vassals/smarthouse.ini" >> /etc/systemd/system/smarthouse-uwsgi.service
systemctl daemon-reload
systemctl start smarthouse-uwsgi 

# END #
