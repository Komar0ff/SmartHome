from flask import Flask, g
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO, emit

app = Flask(__name__) 

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/smarthouse.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
socketio = SocketIO(app)

from models import User

test_user = User(
    email='test@gmail.com',
    password='qwerty', 
    first_name='Test',
    last_name='Test',
    patronymic='Test',
    role='admin'
)

@app.before_first_request
def init_db(): 
    db.create_all() 
   
    db.session.add(test_user) 
    db.session.commit()

@app.before_request
def set_user(): 
    g.user = db.session.query(User).first() 

@app.after_request
def commit_changes(response): 
    db.session.commit()

    return response



from routes.users import bp as users 
from routes.floors import bp as floors
from routes.devices import bp as devices

app.register_blueprint(users, url_prefix='/users') 
app.register_blueprint(floors, url_prefix='/floors')
app.register_blueprint(devices, url_prefix='/devices')
