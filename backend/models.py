from app import db


class User(db.Model): 
    __tablename__ = 'users'
    
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(256), unique=True) 
    password = db.Column(db.String(256)) 
    photo_url = db.Column(db.String(256)) 
    first_name = db.Column(db.String(64)) 
    last_name = db.Column(db.String(64)) 
    patronymic = db.Column(db.String(64))
    role = db.Column(db.String(32)) 

    def serialize(self): 
        return { 
            'id': self.id,
            'email': self.email,
            'password': '*' * len(self.password), 
            'photo_url': self.photo_url,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'patronymic': self.patronymic,
            'role': self.role
        } 

class Floor(db.Model):
    __tablename__ = 'floors'

    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.Integer)
    name = db.Column(db.String(64))
    plan_url = db.Column(db.String(256))
    devices = db.relationship("Device", backref='floor')

    def serialize(self):
        return {
            'id': self.id,
            'number': self.number,
            'name': self.name,
            'plan_url': self.plan_url
        }

class Device(db.Model):
    __tablename__ = 'devices'

    id = db.Column(db.Integer, primary_key=True)
    floor_id = db.Column(db.Integer, db.ForeignKey('floors.id'))
    x_position = db.Column(db.Integer)
    y_position = db.Column(db.Integer)
    type = db.Column(db.String(64))

    def serialize(self):
        return {
            'id': self.id,
            'floor_id': self.floor_id,
            'x_position': self.x_position,
            'y_position': self.y_position,
            'type': self.type
        } 
